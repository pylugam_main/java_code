package practice_Queue;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Stack;

public class DemoQueue {

public static void main(String[] args) {
	
//Queue<Integer>  q =  new LinkedList<Integer>();	
//	
//	q.offer(1);
//	q.offer(2);
//	q.offer(3);
//	
//System.out.println(q.peek());	
//System.out.println(q.poll());	
//System.out.println(q.peek());	
//System.out.println(q.isEmpty());	
//	
	
	
//Stack<Integer> stack = new Stack<Integer>();	
//	
//	stack.add(1);
//	stack.add(2);
//	stack.add(3);
//	
//while(!stack.isEmpty()) {
//	
//	System.out.println(stack.peek());
//	stack.pop();
//	
//	
//}	
	
	
//Deque<Integer> dq = new ArrayDeque<Integer>();
//
//dq.offer(1);
//dq.offer(2);
//dq.offer(3);
//System.out.println(dq);
//
//dq.pollFirst();
//System.out.println(dq.peekFirst());
//System.out.println(dq);
//

List<StudentMarks> marks = new ArrayList<StudentMarks>();

marks.add(new StudentMarks(70, 90));
marks.add(new StudentMarks(50, 80));
marks.add(new StudentMarks(40, 10));
marks.add(new StudentMarks(1000, 69));

PriorityQueue<StudentMarks> spq =
        new PriorityQueue<StudentMarks>(marks);

List<StudentMarks> top3	= new ArrayList<>();
int index = 0 ;

	while(!spq.isEmpty()) { 
        
		if(index == 3) 
			break ;	
			
		top3.add(spq.poll());	
			index ++ ;
		
		
	}
	
	System.out.println(top3);
	
	System.out.println(spq);
	
// passeed paramethod is called total ordering
// implements class is called natural ordering	

PriorityQueue<Integer> pq = new PriorityQueue<Integer>((n1,n2) -> n2 - n1 );	
	
	pq.offer(3);
	pq.offer(4);
	pq.offer(5);
	pq.offer(6);
   pq.offer(1);
	
List<Integer> temp2	= new ArrayList<Integer>();
int index1 = 0 ;

	while(!pq.isEmpty()) { 
        
		if(index1 == 2) 
			break ;	
			
		temp2.add(pq.poll());	
			index1 ++ ;
		
		
	}
	
	System.out.println(temp2);
	
	System.out.println(pq);
	
	
	
// class compareTo  vs interface compare	
	Queue<StudentMarks> spqq =
	        new 
    PriorityQueue<StudentMarks>((a,b) ->
    { 
    	System.out.println("compare is called");	
    	return b.getMaths() - a.getMaths(); 
    	
    });
	
	for(StudentMarks sm : marks) {
		
		spqq.add(sm);
	}
	
	
	
	
}	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

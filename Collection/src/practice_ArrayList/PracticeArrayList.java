package practice_ArrayList;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class PracticeArrayList {

public static void main(String[] args) {
	
List<Integer> alist = new ArrayList<Integer>();	
	
	alist.add(1);
	alist.add(2);
	alist.add(3);
	
//	System.out.println(alist);

//converts array	
	Integer[] arr = alist.toArray(new Integer[0]);
	for(int x : arr) {
		
		System.out.print(x);
	}
	
	
	// replace the elemments in index
//	alist.set(0, 0);
	
	
//copy of first arraylist	
	List<Integer> alist2 = new ArrayList<Integer>(alist);	
	
	alist2.add(4);
    alist2.add(5);
	alist2.add(6);
	
//this method add all elements	
//	alist2.addAll(alist);	
//	System.out.println(alist);
	System.out.println(alist2);
	
	List<Integer> alist3 = alist2.subList(1, 4);
	alist3.set(0, 100);
	System.out.println(alist3);
	
	
	
	
	List<Integer> alist4 = new LinkedList<Integer>();	
	
	
	alist4.add(1);
	alist4.add(5);
	alist4.add(6);
	
ListIterator<Integer> iterators = alist4.listIterator(); 	
	
	System.out.println(iterators.next());
	System.out.println(iterators.next());
	System.out.println(iterators.previous());
	
	
	
	
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

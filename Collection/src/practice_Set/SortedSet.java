package practice_Set;

import java.util.HashSet;
import java.util.NavigableSet;
import java.util.Set;
import java.util.TreeSet;

public class SortedSet {

public static void main(String[] args) {
	
Set<StudentMarks> set3 = new TreeSet<>((s1,s2) -> s1.getMaths() 
		- s2.getMaths() );	
	
	set3.add(new StudentMarks(30, 39));
	set3.add(new StudentMarks(70, 19));
	set3.add(new StudentMarks(90, 29));
	set3.add(new StudentMarks(20, 69));
	set3.add(new StudentMarks(50, 99));
	
	for(StudentMarks x : set3) {
		
		System.out.println(x + " ,");
	}
		
	
//Set<Integer> set = new TreeSet<Integer>();	
//	
//	set.add(2);
//	set.add(1);
//	set.add(9);
//	set.add(6);
//	set.add(4);
//	
//	
//for( int x : set) {
//		
//		System.out.print(x + " ,");
//	}
//	
	
	
	
NavigableSet<Integer> set2 = new TreeSet<Integer>();	

set2.add(2);
set2.add(1);
set2.add(9);
set2.add(6);
set2.add(4);
	
	
for( int x : set2) {
	
	System.out.print(x + " ,");
}

	
System.out.println();
// returns greater or equal to 2 
System.out.println(set2.floor(2));	
// return higher next element 2	
System.out.println(set2.higher(2));	
//return lower next element 2	
System.out.println(set2.lower(2));	

	
	
	
	
	
	
	
	
}	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

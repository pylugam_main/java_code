package multithreading.deadlock;

public class Demolock {

public static void main(String[] args) {
	
	
	String lock1 = "hellow";
	String lock2 = "welcome";
	                                     // 1  2
Thread th1 = new Thread(() -> {
	synchronized (lock1) {

	try {
		Thread.sleep(1);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	synchronized (lock2) {
          System.out.println("lock000 acquired");		
	}
	
	}
	
},"thread1");	
	
	
	
	
	
	
	
Thread th2 = new Thread(() -> {
	synchronized (lock2) {

	try {
		Thread.sleep(2);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	synchronized (lock1) {
          System.out.println("lock111 acquired");		
	}
	}
	                                      // 2  1
},"thread1");	
	
	
  th1.start();	
	
  th2.start();	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	
	
	
	
}	
	
}

package multithreading_practice.sysnchronization;

public class ThreadStarter {

	
	public static void main(String[] args) {
		
		Stack stack = new Stack(5) ;
		
		new Thread(() -> {
			int counter = 0 ;
			while(++counter < 10) {
				System.out.println("pushed" + stack.push(100));
			} 
		},"push").start();
		
		
		new Thread(() -> {
			int counter = 0 ;
			while(++counter < 10) {
				System.out.println("pushed" + stack.pop());
			} 
		},"pop").start();
		
				
		
		
		
		
		
		
		
		
		
		
		
	}
	
	
}

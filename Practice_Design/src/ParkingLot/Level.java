package ParkingLot;

import java.util.ArrayList;
import java.util.List;

public class Level {

	private int floor ;
	private List<ParkingSpot> parkingspots ;
	
	
	public Level(int floor, int numspots) {

	this.floor = floor ;	
    this.parkingspots =  new ArrayList<ParkingSpot>(numspots);
    for(int i = 0 ; i < numspots ; i++)
    	parkingspots.add(new ParkingSpot(i));
    
	}
	
	public boolean parkedVehicle(Vechicle ve) {
		
		for(ParkingSpot spot : parkingspots) {
		if(spot.isAvailable() && ve.getype() == spot.getVehicleType()) {
			  spot.parkedVehicle(ve);
		        return true ;
		}
			
		     
		}
		
	  return false ;	
	} 
	
	public boolean unparkVehicle(Vechicle ve) {
		
		for(ParkingSpot spot : parkingspots) {
		if(spot.isAvailable() && spot.getVehicleType().equals(ve)) {
			spot.unparkVechile();
          return true ;			
		}	
			
		}

	    return false ;
	}
	
	public void disAvailability() {
		
		System.out.print("Level"+ floor+"Availability");
		
		for(ParkingSpot spot : parkingspots) {
			
		System.out.println("Spot" + spot.getspotNumber() + " :" +
		                            (spot.isAvailable() ? "Avaialble" : "not"));	
		
		
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}

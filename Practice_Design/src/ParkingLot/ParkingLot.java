package ParkingLot;

import java.util.ArrayList;
import java.util.List;


public class ParkingLot {

	public static ParkingLot Instance ;
	public  final List<Level> levels ;
	
	public ParkingLot() {	
	levels = new ArrayList<Level>();
	
	}
	
	
	public static ParkingLot getInstance() {
		
		if(Instance == null) {
			
		  Instance = new ParkingLot();	
		}
		
		return Instance ;
	}
	
	
	 public void addLevel(Level level) {
	        levels.add(level);
	    }

		
	public boolean parkvehicle(Vechicle ve) {
	 
		for(Level l : levels) {
			
			
			if(l.parkedVehicle(ve))
			return true ;
		}
		
		return false ;
	
	}
	
	
	
	public boolean unparkVehicle(Vechicle ve) {
		
         for(Level l : levels) {
			
 			if(l.unparkVehicle(ve))
              return true ;
		}
		
		return false ;
		
		
		
	}
	
	
	public void disAvailability() {
		
		for(Level l : levels) {
			
			l.disAvailability();
		}
		
	}
	
	
	
	
	
	
	
	
	
}

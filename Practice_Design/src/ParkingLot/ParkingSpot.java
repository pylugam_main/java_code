package ParkingLot;

public class ParkingSpot {

 private final int spotNumber ;
 private Vechicle parkedVechile ;	
 private VehicleType Vehicletype ;	
	
	
	public ParkingSpot(int spotNumber) {
	
	this.spotNumber = spotNumber;	
	this.Vehicletype = Vehicletype.CAR;
	}
	
	
	public boolean isAvailable() {
		
		return parkedVechile == null ;
	}
	
	public void parkedVehicle(Vechicle ve ) {
		
		if(isAvailable() && ve.getype() == Vehicletype) {
			
			parkedVechile = ve ;
		}
		else {
			
			throw new IllegalArgumentException(" vechile not inserted");
		}
	}
	
	public void unparkVechile() {
		
		parkedVechile = null ;
	} 

	
	public int getspotNumber() {
		
		return spotNumber ;
	}
	
	public VehicleType getVehicleType() {
		
		return Vehicletype ;
	}
	
	public Vechicle getparkedVehicle() {
		
		return parkedVechile ;
	} 
	
	
}

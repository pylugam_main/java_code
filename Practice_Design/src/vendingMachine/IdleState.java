package vendingMachine;

public class IdleState implements VendingMachineState {

	private final vendingMachine vendingmachine ;
	
	public IdleState(vendingMachine vendingmachine) {	
	
		this.vendingmachine = vendingmachine ;
	}
	
	
	@Override
	public void selectProduct(Product product) {
  if(vendingmachine.inventory.isAvailable(product)) {
	  vendingmachine.setSelectedProduct(product);
	  vendingmachine.setState(vendingmachine.getReadyState());
	  System.out.println("Product selected"+ product.getName());
  }
  else {
	  System.out.println("Product not Available");
	  
  }
		
		
	}

	@Override
	public void insertCoin(Coin coin) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void insertNote(Note note) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void despenseProduct() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void returnchange() {
		// TODO Auto-generated method stub
		
	}

}

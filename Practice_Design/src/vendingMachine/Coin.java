package vendingMachine;

public enum Coin {

	PENNY(0.01),
	QUERTER(0.25),
	DIME(0.1),
	NICKAL(0.05) ;
	
	 private final double value ;
	
	Coin(double value){
		
	this.value = value ;
	
	}
	
	public double getValue() {
		
		return value ;
	}
	
}

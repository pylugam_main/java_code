package vendingMachine;

public class vendingMachine {
 
 private static vendingMachine instance;
 Inventory inventory ;
 private final VendingMachineState idleState ;
 private final VendingMachineState readyState ;
 private final VendingMachineState dispenceState ;	
 private final VendingMachineState returnChangeState ;	
 private VendingMachineState currrentState ; 	
 private Product SelectedProduct ;	
 private double totalPayment ;	

	
    private vendingMachine(){
  
    	this.inventory = new Inventory();
    	this.idleState = new IdleState(this);
    	this.readyState = new ReadyState(this); 
    	this.dispenceState = new DespenseState(this); 
    	returnChangeState = new RerturnChangeState(this);
    	this.currrentState = idleState ; 
    	SelectedProduct = null ;
    	totalPayment = 0.0 ;
	
 }	

 public static synchronized vendingMachine getInstance() {
	 
	 if(instance == null) {
		 
		 instance = new vendingMachine();
	 }
	 
	 return instance ;
	 
 }
  

 public void selectProduct(Product product) {
      currrentState.selectProduct(product);
 }


 public void insertCoin(Coin coin) {
         currrentState.insertCoin(coin);
 }

 public void insertNote(Note note) {
     currrentState.insertNote(note);
 }

 public void dispenseProduct() {
currrentState.despenseProduct();
 }

 public void returnChange() {
	 currrentState.returnchange();
 }

 void setState(VendingMachineState state) {
     currrentState = state;
 }

 VendingMachineState getIdleState() {
     return idleState;
 }

 VendingMachineState getReadyState() {
     return readyState;
 }

 VendingMachineState getDispenseState() {
     return dispenceState ;
 }

 VendingMachineState getReturnChangeState() {
     return returnChangeState;
 }

 Product getSelectedProduct() {
     return SelectedProduct; 
 }

 void setSelectedProduct(Product product) {
	 SelectedProduct = product;
 }

 void resetSelectedProduct() {
	 SelectedProduct = null;
 }

 double getTotalPayment() {
     return totalPayment;
}

 void addCoin(Coin coin) {
     totalPayment += coin.getValue();
 }

 void addNote(Note note) {
     totalPayment += note.getValue();
 }

 void resetPayment() {
     totalPayment = 0.0;
 }

	
	
}

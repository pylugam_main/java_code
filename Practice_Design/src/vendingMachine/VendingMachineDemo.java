package vendingMachine;

import vendingmachine.Product;

public class VendingMachineDemo {

	public static void main(String[] args) {
		
		
		vendingMachine v = vendingMachine.getInstance();
		
		      Product coke = new Product("Coke", 1);
	        Product pepsi = new Product("Pepsi", 1);
	        Product water = new Product("Water", 1.0);

	        v.inventory.addProduct(coke, 5);
	        v.inventory.addProduct(pepsi, 3);

		v.selectProduct(coke);
		
		v.insertNote(Note.FIVE);
		
		v.dispenseProduct();
		
		v.returnChange();
		
		
	}
	
	
}

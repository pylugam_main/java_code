package vendingMachine;

public class ReadyState implements VendingMachineState {

	private final vendingMachine vendingmachine ;
	
	public ReadyState(vendingMachine vendingmachine) {	
	
		this.vendingmachine = vendingmachine ;
	}
	
	
	@Override
	public void selectProduct(Product product) {
		
	}

	@Override
	public void insertCoin(Coin coin) {
		vendingmachine.addCoin(coin);
		System.out.println("Coin inserted"+ coin);
		checkPaymentStatus();
	}

	@Override
	public void insertNote(Note note) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void despenseProduct() {
		// TODO Auto-generated method stub
		
	}

  private void checkPaymentStatus() {
	  
	if(vendingmachine.getTotalPayment() >=  vendingmachine.getSelectedProduct().getPrice()) {
		vendingmachine.setState(vendingmachine.getDispenseState());
	}  
	  
  }


@Override
public void returnchange() {
	// TODO Auto-generated method stub
	
}
  
}

package CofeevendingMachine;

import java.util.Map;

public class Coffee {

 private final String name ;	
 private final double price ;	
 private final Map<Ingrediant, Integer> recipe ;	
	
	
public Coffee(String name,double price,Map<Ingrediant, Integer> recipe) {
	
	this.name = name ;
	this.price = price ;
	this.recipe = recipe ;
}


public String getName() {
	return name;
}


public double getPrice() {
	return price;
}


public Map<Ingrediant, Integer> getRecipe() {
	return recipe;
}	
	
	
	
	
	
	
	
}

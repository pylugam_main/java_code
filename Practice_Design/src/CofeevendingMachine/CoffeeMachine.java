package CofeevendingMachine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CoffeeMachine {

 private static final CoffeeMachine instance = new CoffeeMachine();	
 private final List<Coffee> coffeeMenu ;	
 private final Map<String, Ingrediant> ingrediants ;	
	
	
	public CoffeeMachine() {
    	
	coffeeMenu = new ArrayList<Coffee>();
	ingrediants = new HashMap<String, Ingrediant>();
	
	initializeIngredients();
    initializeCoffeeMenu();

	}

	public static CoffeeMachine getInstance() {
		
		return instance ;
	}
	

	private void initializeCoffeeMenu() {

		Map<Ingrediant, Integer> espreessoRecipe = new HashMap<Ingrediant, Integer>();
		
		espreessoRecipe.put(ingrediants.get("Coffee"),1);
		espreessoRecipe.put(ingrediants.get("Water"),1);
		coffeeMenu.add(new Coffee("espreesso", 2.3, espreessoRecipe));
		
Map<Ingrediant, Integer> latteRecipe = new HashMap<Ingrediant, Integer>();
		
latteRecipe.put(ingrediants.get("Coffee"),1);
latteRecipe.put(ingrediants.get("Water"),1);
latteRecipe.put(ingrediants.get("Milk"),1);
		coffeeMenu.add(new Coffee("espreesso", 5.4, latteRecipe));	
		
Map<Ingrediant, Integer> cappuccinoRecipe = new HashMap<Ingrediant, Integer>();
		
        cappuccinoRecipe.put(ingrediants.get("Coffee"),1);
        cappuccinoRecipe.put(ingrediants.get("Water"),1);
        cappuccinoRecipe.put(ingrediants.get("Milk"),1);
		coffeeMenu.add(new Coffee("espreesso", 5.4, cappuccinoRecipe));
	}


	private void initializeIngredients() {

		ingrediants.put("Coffee", new Ingrediant("Coffee", 10)) ;
		ingrediants.put("Water", new Ingrediant("Water", 10)) ;
		ingrediants.put("Milk", new Ingrediant("Milk", 10)) ;


	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

package CofeevendingMachine;

public class Ingrediant {

 private String name ;
 private int quantity ;	
	
	
	public String getName() {
	return name;
}


public int getQuantity() {
	return quantity;
}


	public Ingrediant(String name ,int quantity ) {	
	
	this.name = name ;
	this.quantity = quantity ;
	
	}
	
	
	public synchronized void updateQuality(int amount) {
		
		quantity += amount ;
	}
	
	
	
	
	
	
}

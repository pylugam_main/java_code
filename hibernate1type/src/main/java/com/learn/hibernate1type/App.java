package com.learn.hibernate1type;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        
        
      Student s = new Student() ; 
        
        s.setId(3);
        
        s.setMarks(346);
        	
      s.setName("hellow");        
        
        
  Configuration con = new Configuration().configure().addAnnotatedClass(Student.class) ;  
        
  StandardServiceRegistryBuilder reg = new StandardServiceRegistryBuilder().applySettings(con.getProperties()) ;      
        
  SessionFactory sf = con.buildSessionFactory(reg.build());      
        
  Session session = sf.openSession();
        
  Transaction tx = session.beginTransaction();
  
        session.save(s) ;
        
      tx.commit();        
        
    }
}

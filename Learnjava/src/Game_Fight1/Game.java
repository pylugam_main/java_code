package Game_Fight1;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.util.Random;

public class Game extends Canvas implements Runnable {

	
	public static final int WIDTH = 640 , HEIGHT = WIDTH / 12 * 9 ;
	
	private Thread thread ;
	
	private boolean running = false ; 
		
	private Random r ;
	
	private Handler handler ;
	
    private HUD hud ; 

	public Game() {
		
		handler = new Handler() ;
		
		this.addKeyListener(new KeyInput(handler));

		
		new Window(WIDTH, HEIGHT, "Let's Buid Game" , this) ;
		
		hud = new HUD() ;
		
		r = new Random() ;
		
			
	handler.addobject(new Player(WIDTH/2-32,HEIGHT/2-32, ID.Player,handler));
	//for(int i= 0 ; i < 20 ; i++)
	handler.addobject(new BasicEnemy(r.nextInt(WIDTH),r.nextInt(HEIGHT), ID.BasicEnemy,handler));
	handler.addobject(new BasicEnemy(r.nextInt(WIDTH),r.nextInt(HEIGHT), ID.BasicEnemy,handler));
	handler.addobject(new BasicEnemy(r.nextInt(WIDTH),r.nextInt(HEIGHT), ID.BasicEnemy,handler));
	handler.addobject(new BasicEnemy(r.nextInt(WIDTH),r.nextInt(HEIGHT), ID.BasicEnemy,handler));

	//handler.addobject(new Player(WIDTH/2+64,HEIGHT/2-32, ID.Player2));
	

	}
public synchronized void start() {
	
	thread = new Thread(this);
	thread.start();
	running = true ;
}	

public synchronized void stop() {
	
	try {
		
		thread.join();
		
		running = false ;
		
	}catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
	}
}	

public static int clamp(int var,int min,int max) {
	
	if(var >= max )
		return var = max ;
	else if(var <= min)
		return var = min ;
	else
		return var ;
	
}
	public static void main(String[] args) {
		
		new Game() ;
	}

	
	@Override
	public void run() {
		// TODO Auto-generated method stub
	
      long lastTime = System.nanoTime() ;		
	   
      double amountofTicks = 60.0 ;
		
	double ns = 1000000000 / amountofTicks ;	
		
	double delta = 0 ;	
	
	long timer = System.currentTimeMillis() ;
	
	int frames = 0 ;

	while(running) {
		
		long now = System.nanoTime() ;
		
		delta += (now - lastTime) / ns ;
		
		lastTime = now ;
		
		
		while(delta >= 1) {
			
			tick() ;
	
			delta-- ;
			
		}
		
		if(running)
			render() ;
		frames++ ;
		
		if(System.currentTimeMillis() - timer > 1000) {
			
			timer += 1000 ;
			
		//	System.out.println("FPS: " + frames);
			
			frames = 0 ;
		}
		
	
	}
	
	stop();
	
		}
	private void tick() {
		// TODO Auto-generated method stub
		
		handler.tick();
		
		hud.tick(); 
		
	}
	private void render() {
		// TODO Auto-generated method stub
		
	BufferStrategy bs = this.getBufferStrategy() ;
	
	if(bs == null) {
		
		this.createBufferStrategy(3);
		return ;
		
	}
		
		Graphics g = bs.getDrawGraphics() ;
		
		
		
		g.setColor(Color.black);
		
		
		g.fillRect(0, 0, WIDTH, HEIGHT);
				
		handler.render(g);
		
		hud.render(g);

		
		g.dispose();
		
		bs.show() ;
		
	}
	
}

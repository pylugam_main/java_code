package Game_Fight1;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;

public class BasicEnemy  extends GameObject{

	private Handler handler ;
	public BasicEnemy(int x, int y, ID id,Handler handler) {
		super(x, y, id);
		// TODO Auto-generated constructor stub
		
		 velX = 5 ;
		
		  VelY = 5 ;
		  
		  
		  this.handler = handler ;
	}

public Rectangle getBounds() {
		
		
		return new Rectangle(x,y,16,16);
	}

	@Override
	public void tick() {
		// TODO Auto-generated method stub
		
		x += velX ;
		
		y += VelY ;
		
		if(y <= 0 || y >= Game.HEIGHT) VelY *= -1 ;
		
		if(x <= 0 || x >= Game.WIDTH - 16) velX *= -1 ;
		
handler.addobject(new Trail(x, y, ID.Trail, Color.red, 16, 16,0.01f, handler));


	}

	@Override
	public void render(Graphics g) {
		// TODO Auto-generated method stub
		
		
Graphics2D g2d = (Graphics2D) g ;
		
	  g.setColor(Color.red);
	  
	  g.fillRect(x, y,16,16);
		
		
	}

	
	
	
	
}

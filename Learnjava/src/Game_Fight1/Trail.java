package Game_Fight1;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;

public class Trail extends GameObject{
	
	private float life ;
	
	private float alpha = 1 ;

	private Handler handler ;
	
	private Color color ;
	
	private int width,height;
	
	public Trail(int x, int y, ID id,Color color,int Width,int height,float life,Handler handler) {
		super(x, y, id);
		// TODO Auto-generated constructor stub
		
		this.color = color;
		
		this.width = width ;
		
		this.height = height ;
		
		this.life = life ;
		
		this.handler = handler ; 
		
	}

	@Override
	public void tick() {
		// TODO Auto-generated method stub
		
		if(alpha > life)
			alpha -=(life - 0.001f) ;
		else 
		     handler.removeobject(this);
		
	}

	@Override
	public void render(Graphics g) {
		// TODO Auto-generated method stub
		
		Graphics2D g2d = (Graphics2D) g ;
		
		g2d.setComposite(makeTransparaent(alpha));
		
		g.setColor(color);
		
		g.fillRect(x, y, width, height);

		g2d.setComposite(makeTransparaent(1));
		
	}

private AlphaComposite  makeTransparaent(float alpha) {
		
		int type = AlphaComposite.SRC_OVER ;
		
		return (AlphaComposite.getInstance(type,alpha)) ;
		
}

	public Rectangle getBounds(){
		
		return null;
	}
	
	

	

}

package interface_;

public class Comparable_demo implements Comparable {
	int age ;
    String name ;
    int mark ;
    
	public Comparable_demo(int age, String name, int mark) {
		
		this.age = age;
		this.name = name;
		this.mark = mark;
	}

	public static void main(String[] args) {
      		// TODO Auto-generated method stub

		Comparable_demo jan = new Comparable_demo(189,"kannan", 80);
		Comparable_demo feb = new Comparable_demo(18,"pransanth", 90);
      
		     int result = feb.compareTo(jan);
	
             	 System.out.println(result);
	}

	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		      Comparable_demo jan = (Comparable_demo) o ;
		      if(this.age < jan.age) {
		    	  return +1;
		      }
		      else if(this.age>jan.age){
		    	  return -1 ;
		      }
		      
		      return 0;
	}

	
}

package interface_;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class InterfaceList1 implements Comparable<InterfaceList1> {

	String name ;
	
	int age ;
	
	int score ;
	
	
	public InterfaceList1(String name, int age, int score) {
		
		this.name = name;
		this.age = age;
		this.score = score;
	}


	@Override
	public int compareTo(InterfaceList1 o) {
		
		if(o.age > this.age)
			return +1 ;
		else if (o.age < this.age)
			return -1 ;
		
		// TODO Auto-generated method stub
		return 0;
	}

	
	public static void main(String[] args) {
		
		
		
ArrayList<InterfaceList1> ll = new ArrayList<InterfaceList1>() ;

             ll.add( new InterfaceList1("kannan", 17, 20)) ;

             ll.add(new InterfaceList1("john", 40, 60)) ;
	
     		Collections.sort(ll); 
  
    for(InterfaceList1 o : ll) {
    	
    	System.out.println(o.age + o.name + o.score);
    }         
		
	}
}

package test_String_Array1;

public class ArrayTest {

public static int submin(int[]arr , int n) {
	
	int[] dp = new int[n] ;
	
	dp[0] = arr[0] ;
    	
	dp[1] = arr[1] ;
	
	dp[2] = arr[2] ;
	
	dp[3] = arr[3] ;
	
	for(int i = 4 ; i < arr.length ; i++) {
		
		dp[i] = arr[i] + Math.min(Math.min(dp[i - 1], dp[i - 2]),
				Math.min(dp[i - 3], dp[i - 4])) ;
	}
	
	return Math.min(Math.min(dp[n - 1], dp[n - 2]), 
			Math.min(dp[n- 3],dp[n - 4])) ;
	
}

public static int printminandins(String A1 , String A2 , int n1 ,int n2) {
	
	int[][] l = new int [n1 + 1][n2 + 1]  ;
	
	for(int i = 0 ; i <= n1 ; i++) {
		
		for(int j = 0 ; j <= n2 ; j++) {
			
			if(i == 0 || j == 0)
				l[i][j] =0 ;
			
			else if(A1.charAt(i - 1 ) == A2.charAt(j - 1))
			l[i][j] = l [i - 1] [j - 1] + 1;
			
			else
				l[i][j] = Math.max(l[i - 1][j], l[i][j - 1]) ;
		}
	}
	
	
	return l[n1][n2]  ;
}


public static void  printins() {
	
	String A1 = "hheap" ;
	
	String A2 = "pea" ;
	
   int n1 = A1.length() ;
	
   int n2 = A2.length() ;
   

	int len = printminandins(A1, A2, n1, n2) ;
	
	System.out.println(n1 - len);
	
	System.out.println(n2 - len);
	
}


public static void main(String[] args) {
		// TODO Auto-generated method stub
	
	int[] arr = {1,2,3,3,4,5,6,1};

	int n = arr.length ;
	
//	System.out.println(submin(arr, n));
	
    printins();
   
	}

}

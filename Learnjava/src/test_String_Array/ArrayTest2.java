package test_String_Array;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class ArrayTest2 {

public static void factorial() {
	
	int N = 5 ;
	
	ArrayList<Integer> ll = new ArrayList<Integer>() ;
	
	int[] factorial = new int [1000] ;
	 
	factorial[0] = 1 ;
	  
	int size = 1 ;
	
	for(int i = 2 ; i <= N ; i++) {
		size = multi(i , factorial , size) ;
	}
	
	for(int i = size - 1 ; i >= 0 ; i--) {
		
	             ll.add(factorial[i]) ;
	}
	
	for(int num : ll) {
		
		System.out.print(num);
	}
	
	
	
	
}	

public static int multi(int x , int[] arr , int size) {
	
	int carry = 0 ;
	
	for(int i = 0 ; i < size ; i++) {
		
		int prod = arr[i] * x + carry ;
		
		arr[i] = prod % 10 ;
		
		carry = prod / 10 ;
		
	}

	while(carry != 0) {
		
		arr[size] = carry % 10 ;
	     	
		carry = carry / 10 ;
		
		size++ ;
		
	}
	
         return size ;
	
}

public static int smallinteger() {
	
	int[] a = {2,3,-4,-6,4};
	
	int n = a.length ;
	
	int swift = segarate(a,n) ;
	
	int[] arr = new int [n - swift] ;
	   
	int j = 0 ;
	
	for(int i = swift ; i < n ; i++) {
		
		arr[j] = a[i] ;
		
		  j++ ;
	}
	
	for(int i = 0 ; i < arr.length ; i++ ) {
		
		int x = Math.abs(arr[i]) ;
		
	if(x - 1 < arr.length && arr[x - 1] < 0)
		arr[x - 1] = - arr[x - 1] ;
	
	
	}
	
	for(int i = 0 ; i < arr.length ; i++) {
		
		if(arr[i] > 0) {
			 return i + 1 ;
		}
	}
	
	
	 return arr.length + 1 ;
    	
}

public static int segarate(int[] arr , int n) {
	
	int j = 0;
	
	for(int i = 0 ; i < n ; i++) {
		
		if(arr[i] <= 0) {
			
			int temp = arr[i] ;
			
			arr[i] = arr[j] ;
			
			arr[j] = temp ;
			
			j++ ;
		}
 	}
	
	  return j ;
	
}

public static void commomarray() {
	
	int[] A = {20,80,2,7,3,6,5,2,3};
	
	int[] B = {2,3,4,6,7,8,20,80} ;
	
	int[] C = {20,4,5,6,7,3,4,80};
	
	int n1 = A.length ;
	
	 int n2 = B.length ;
	 
	 int n3 = C.length ;
	 
	 int i = 0 , j = 0 , k = 0 ;
	 
	 while(i < n1 && j < n2 && k < n3) {
		 
		 if(A[i] == B[j] && B[j] == C[k]) {
			 
			 System.out.print(A[i]);
			 
			 i++ ;
			 
			 j++ ;
			 
			 k++ ;
			 

		 }
		 
		 else if(A[i] < B[j]) {
			 
			 i++ ;
		 }
		 
		 else if (B[j] < C[k]) {
			 
			 j++ ;
		 }
		 
		 else {
			 
			 k++ ;
		 }
	 }
	
}

public static boolean issubset() {
	
	int[] a = {1,2,3,4};
	
	int[] b = {1,2,3,4};
	
	HashSet<Integer> ll = new HashSet<Integer>() ;
	
	
	for(int num : a) {
		
		ll.add(num) ;
	}
	
	
	for(int num1 : b) {
		
		if(!(ll.contains(num1)))
			return false ;
	}
	
	return true ;
}

public static boolean zerosum() {
	
	int[] a = {1,2,3,4,5,6,8} ;
	
	HashSet<Integer> ll = new HashSet<Integer>() ;
	
	int sum = 0 ;
	
	for(int num : a) {
		
		sum += num ;
		
		if(ll.contains(sum) || sum == 0)
		       return true ;
		
		ll.add(sum) ;
	}
	
	  return false ;
}

public static int missing() {
	  
	  int[] N = {1,2,3,5};
	  
	  int n = N.length + 1 ;
	  
	  int total = (n * (n + 1)) / 2 ;
	  
	  int sum = 0 ;
	  
	  for(int num : N) {
		  
		  sum += num ;
	  }
	  
	  return total - sum ;
	
}

public static int arraysum() {
	
	int[] A = {1,2,3,4,5,3};
	
	int presum = A[0] ;
	
	int sum = A[0] ;
	
	for(int i = 1 ; i < A.length ; i++) {
		
		presum = Math.max(presum, A[i] + presum) ;
		
		sum = Math.max(sum, presum) ;
		
	}
	
	return presum ;
	
}

public static void nonrepeat() {
	
	int[] A = {1,2,3,4,5,2};
	
	HashMap<Integer, Integer> ll = new HashMap<Integer, Integer>() ;
	
	for(int i = 0 ; i < A.length ; i++) {
		
		ll.put(A[i],ll.getOrDefault(A[i], 0) + 1) ;
		
	}
	
	for(int i = 0 ; i < A.length ; i++) {
		
		if( ll.get(A[i]) == 1) {
			System.out.print(A[i] + "  ");
		}
	}
	
	 // return -1 ;
	  
}

public static void firstrepeat() {
	
	int[] A = {1,2,3,4,5,2};

	HashMap<Integer, Integer> ll = new HashMap<Integer, Integer>() ;
	
	int sum = Integer.MAX_VALUE;
	
	for(int i = 0 ; i < A.length ; i++) {
		
		if(ll.containsKey(A[i])) {
			
		sum = Math.min(sum,A[i]) ;
		
		}
		else {
			ll.put(A[i],i + 1) ;
		}
	}
	
	if(Integer.MAX_VALUE == sum) {
		
		System.out.println(-1);
	}
	else
	  System.out.println(sum);
}


public static void rotate() {
	
	int[] A = {1,2,3,4,5};
	
	int n = A.length ;
	
	int last = A[A.length - 1] ;
	
	for(int i = A.length - 1 ; i > 0 ; i--) {
		
		A[i] = A[i - 1] ;
	}
	
	A[0] = last ;
	
	
	for(int num : A) {
		
		System.out.print(num);
	}
}

public static void negative() {
	
	int[] A = {11,-5,33,-8,2,3,4-4} ;
	
	int j = 0 ;
	
	for(int i = 0 ; i < A.length ; i++) {
		
		if(A[i] >= 0) {
			
			if( i != j) {
				
				int temp = A[i] ;
				
				A[i] = A[j] ;
				
				A[j] = temp ;
				
			}
			
			j++ ;
		}
	}
	
	for(int num : A) {
		
		System.out.print(num);
	}
	
}
  public static void main(String[] args) {
	
//	  factorial();

//	  System.out.println(smallinteger());
//
//	  System.out.println(issubset());
//	  commomarray();
//	  
//	  System.out.println(zerosum());
	  
//	  System.out.println(missing());
	  
	//  System.out.println(arraysum());
	  
	//  nonrepeat();
	  
	//  firstrepeat();
	  
	//  rotate();
	  
//	  negative();
}

}
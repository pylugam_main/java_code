package test_String_Array;

import java.util.ArrayList;
import java.util.HashMap;

public class ArrayTest3 {

public static void rotate() {
	
	int[] a = {1,2,3,4,5};
	
	//ArrayList<Integer> kk = new ArrayList<Integer>() ;
	
	int n = a.length ;
	
	int last = a[n -1] ;
	
	for(int i = n-1 ; i > 0 ; i--) {
		
		a[i] = a[i - 1] ;
	}
  
	 a[0] = last ;
	 
	 for(int num : a) {
		 
		 System.out.print(num);
	 }
}


public static int first() {
	
	int[] num = {1,2,3,2,5,6,7};
	
	HashMap<Integer, Integer> ll = new HashMap<Integer, Integer>() ;
	
	int min = Integer.MAX_VALUE ;
	
	for(int i = 0 ; i < num.length ; i++) {
		
		if(ll.containsKey(num[i])) {
			
			min = Math.min(min, ll.get(num[i])) ;
		}
		
		else {
			
			ll.put(num[i], i+ 1) ;
		}
	}
	
	if(min == Integer.MAX_VALUE) {
		
		return -1 ;
	}
	else
	return min ;
} 

public static int nonrepeating() {
	
	int[] a = {1,2,2,3,8};
	
	int[] b = new int[3] ;
	
	int j = 0 ;
	
	 HashMap<Integer, Integer> ll = new HashMap<Integer, Integer>() ;
	  
	 int n = a.length ;
	 
	 for(int i = 0 ; i < n ; i++ ) {
		 
		 ll.put(a[i], ll.getOrDefault(a[i],0) + 1 ) ;
	 }
	 
    for(int i = 0 ; i < n ; i++) {
    	
    	if(ll.get(a[i]) == 2) {
    		
    	b[j] =  a[i];
    	
    	  j++ ;
    	}
    	
    }
    
    for(int num : b) {
    	
    	 System.out.print(num);
    }
	
   return 0 ;
} 

public static void negative() {
	
	int[] a = {11,-5,33,-8};
	
	int n = a.length ;
	
	int j = 0 ;

	
	for(int i = 0 ; i < n ; i++) {
				
		if(a[i] >= 0) {
		if( i != j ) {
			
			int temp = a[i] ;
			
			a[i] = a[j] ;
			
			a[ j] = temp ;
		}
		
		j++ ;
	}
		
	}
	for(int num : a) {
		
		System.out.print(num);
	}
 }
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		 //  rotate();
		   
		//   System.out.print(first());
		   
          //   nonrepeating() ;	
             
             negative();
	}

}




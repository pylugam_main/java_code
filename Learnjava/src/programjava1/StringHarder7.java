package programjava1;

public class StringHarder7 {

	
	static int minOperation(int k)
	{
	     
	    // dp is initialised
	    // to store the steps
	    int dp[] = new int[k + 1];
	 
	    for(int i = 1; i <= k; i++)
	    {
	       dp[i] = dp[i - 1] + 1;
	        
	       // For all even numbers
	       if (i % 2 == 0)
	       {
	           dp[i] = Math.min(dp[i], dp[i / 2] + 1);
	       }
	    }
	    return dp[k];
	}
	 	

	 static int maxProd(int n)
	    {
	        // Base cases
	        if (n == 0 || n == 1) return 0;
	 
	        // Make a cut at different places
	        // and take the maximum of all
	        int max_val = 0;
	        for (int i = 1; i < n; i++)
	        max_val = Math.max(max_val,
	                  Math.max(i * (n - i),
	                   maxProd(n - i) * i));
	 
	        // Return the maximum of all values
	        return max_val;
	    }   
	 

	 static int printCountRec(int dist)
	    {
	        // Base cases
	        if (dist<0)    
	            return 0;
	        if (dist==0)    
	            return 1;
	 
	        // Recur for all previous 3 and add the results
	        return printCountRec(dist-1) + 
	               printCountRec(dist-2) +
	               printCountRec(dist-3);
	    }
	 

	  static int lcs(String str1, String str2, int m, int n)
	    {
	        int L[][] = new int[m + 1][n + 1];
	        int i, j;
	 
	        // Following steps build L[m+1][n+1] in
	        // bottom up fashion. Note that L[i][j]
	        // contains length of LCS of str1[0..i-1]
	        // and str2[0..j-1]
	        for (i = 0; i <= m; i++) {
	            for (j = 0; j <= n; j++) {
	                if (i == 0 || j == 0)
	                    L[i][j] = 0;
	 
	                else if (str1.charAt(i - 1)
	                         == str2.charAt(j - 1))
	                    L[i][j] = L[i - 1][j - 1] + 1;
	 
	                else
	                    L[i][j] = Math.max(L[i - 1][j],
	                                       L[i][j - 1]);
	            }
	        }
	 
	        // L[m][n] contains length of LCS
	        // for X[0..n-1] and Y[0..m-1]
	        return L[m][n];
	    }
	 
	    // function to find minimum number
	    // of deletions and insertions
	    static void printMinDelAndInsert(String str1,
	                                     String str2)
	    {
	        int m = str1.length();
	        int n = str2.length();
	 
	        int len = lcs(str1, str2, m, n);
	 
	        System.out.println("Minimum number of "
	                           + "deletions = ");
	        System.out.println(m - len);
	 
	        System.out.println("Minimum number of "
	                           + "insertions = ");
	        System.out.println(n - len);
	    }
	 	 
	    static int minSum(int[] arr, int n) 
	    { 
	        // dp[i] is going to store minimum sum 
	        // subsequence of arr[0..i] such that arr[i] 
	        // is part of the solution. Note that this 
	        // may not be the best solution for subarray 
	        // arr[0..i] 
	        int[] dp = new int[n]; 
	  
	        // If there is single value, we get the 
	        // minimum sum equal to arr[0] 
	        if (n == 1) 
	            return arr[0]; 
	  
	        // If there are two values, we get the 
	        // minimum sum equal to the minimum of 
	        // two values 
	        if (n == 2) 
	            return Math.min(arr[0], arr[1]); 
	  
	        // If there are three values, return 
	        // minimum of the three elements of 
	        // array 
	        if (n == 3) 
	        return Math.min(arr[0], Math.min(arr[1], arr[2])); 
	  
	        // If there are four values, return minimum 
	        // of the four elements of array 
	        if (n == 4) 
	            return Math.min(Math.min(arr[0], arr[1]), 
	                            Math.min(arr[2], arr[3])); 
	  
	        dp[0] = arr[0]; 
	        dp[1] = arr[1]; 
	        dp[2] = arr[2]; 
	        dp[3] = arr[3]; 
	  
	        for (int i = 4; i < n; i++) {
	        	 
       int x = arr[i] + Math.min(Math.min(dp[i - 1], dp[i - 2]), 
               Math.min(dp[i - 3], dp[i - 4])); 
       
	        	 dp[i] = arr[i] + Math.min(Math.min(dp[i - 1], dp[i - 2]), 
                         Math.min(dp[i - 3], dp[i - 4]));     	
	        }
	      	  
	        // Return the minimum of last 4 index 
	        return Math.min(Math.min(dp[n - 1], dp[n - 2]), 
	                        Math.min(dp[n - 4], dp[n - 3])); 
	    } 
	    
	    
public static void main(String[] args) {
	
// Minimize steps to reach K from 0 by adding 1 or doubling at each step
	
	
//	int K = 12;
//    System.out.print( minOperation(K));
	
    // Maximum Product Cutting 
    
  //  System.out.println("Maximum Product is "
   //         + maxProd(10));
    
//Count number of ways to cover a distance
    
//    int dist = 4;
//    System.out.println(printCountRec(dist));
    
    
    //  Minimum number of deletions and insertions to
    //  transform one string into another

//    String str1 = new String("hheap");
//    String str2 = new String("pea");
   
      // Function Call
 //  printMinDelAndInsert(str1, str2);

    //Minimum sum subsequence such that at least one
    // of every four consecutive elements is picked

    int[] arr = { 1, 2, 3, 3, 4, 5, 6, 1 }; 
    int n = arr.length; 
    System.out.println(minSum(arr, n)); 
}	
	
	
	
}

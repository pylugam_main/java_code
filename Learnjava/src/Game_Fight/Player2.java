package Game_Fight;

public class Player2 extends Player1 {

	private int health ; 
	
	private  Boolean armor ;

	public Player2(String name, String weapon, int health, Boolean armor) {
		super(name, weapon, health);
	   this.health = health;
		this.armor = armor;
	}

	@Override
	public void damagebyGun1() {
		// TODO Auto-generated method stub
	
	  if(armor){
		  
		  this.health -=20;
		  
		  if(this.health<=0) this.health = 0 ;
		  
		  System.out.println("Armor is on, got hit by gun 1,health is "
		  		+ "reduced by 20,"+"New health is "+this.health); 
	  }
	
	   if(!armor) {
		   
		   this.health -=30;
			  
			  if(this.health<=0) this.health = 0 ;
			  
			  System.out.println("Armor is off, got hit by gun 1,health is "
			  		+ "reduced by 30,"+"New health is "+this.health); 
		   
		   
		     }
	      
	    if(this.health == 0) {
	    	
	    	System.out.println(getName()+"is dead ");
	    } 

	}

	@Override
	public void heal() {
		// TODO Auto-generated method stub
		super.heal();
	}

	@Override
	public void damagebyGun2() {
		// TODO Auto-generated method stub

		 if(armor){
			  
			  this.health -=40;
			  
			  if(this.health<=0) this.health = 0 ;
			  
			  System.out.println("Armor is on, got hit by gun 1,health is "
			  		+ "reduced by 40,"+"New health is "+this.health); 
		  }
		
		   if(!armor) {
			   
			   this.health -=50;
				  
				  if(this.health<=0) this.health = 0 ;
				  
				  System.out.println("Armor is on, got hit by gun 1,health is "
				  		+ "reduced by 50,"+"New health is "+this.health); 
			   
			   
			     }
		      
		    if(this.health == 0) {
		    	
		    	System.out.println(getName()+"is dead ");
		    } 
	
	
	
	}
	
	
	
	
	
	
}

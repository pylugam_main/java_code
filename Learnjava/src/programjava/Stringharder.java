package programjava;

import java.util.*;

public class Stringharder {
	
	public static void romanToInt(String str) {
		
		HashMap<Character,Integer> ll = new HashMap<>();
		
		ll.put('I',1) ;
		
		ll.put('V',5) ;
		
		ll.put('X',10) ;
		
		ll.put('C',100) ;
		
		ll.put('L',50) ;
		
		ll.put('D',500) ;
		
		ll.put('M',1000) ;
		
		
		int total = 0 ;
		int prevalue = 0 ;
		
		
		for(int i =str.length()-1 ; i >=0 ; i--){
			
			int value =ll.get(str.charAt(i)) ;
			
			
			if(value < prevalue){
				total -= value ;
			}
			else{
				
				total += value ; 
			}
			
			
			prevalue =  value ;
			
		}
		
		System.out.println(total);  
		
		
		
		
	}
	
	

    public static void convertToRoman(int n) {
    	
        int[] values = {
            1000, 900, 500, 400,
            100, 90, 50, 40,
            10, 9, 5, 4, 1
        };
        
        String[] symbols = {
            "M", "CM", "D", "CD",
            "C", "XC", "L", "XL",
            "X", "IX", "V", "IV", "I"
        };
        
        StringBuilder result = new StringBuilder();
        int i = 0;
        
        while (n > 0) {
            while (n >= values[i]) {
                n -= values[i];
                result.append(symbols[i]);
            }
            i++;
        }
        
        System.out.println(result);
    }	
	
	
public static void minDistance(String[] words, String word1, String word2){
        int minDistance = Integer.MAX_VALUE;
        int index1 = -1;
        int index2 = -1;
        
        for (int i = 0; i < words.length; i++) {
            if (words[i].equals(word1)) {
                index1 = i;
                if (index2 != -1) {
                    minDistance = Math.min(minDistance, Math.abs(index1 - index2));
                }
            } else if (words[i].equals(word2)) {
                index2 = i;
                if (index1 != -1) {
                    minDistance = Math.min(minDistance, Math.abs(index1 - index2));
                }
            }
        }
        
        System.out.println(minDistance); 
   }


public static void findPeakElement(int[] arr, int n) {
	
	// only returns index value 
	
    int left = 0;
  int right = n - 1;

while (left < right) {
int mid = left + (right - left) / 2;
if (arr[mid] > arr[mid + 1]) {
    // Peak element is in the left half
    right = mid;
} else {
    // Peak element is in the right half
    left = mid + 1;
}
}

// left will be the peak element index
System.out.println(left);
  //int index = (left == 2) ? 1 : 0 ;
  // System.out.println(index );

}


public static void findMinMax(int[] arr, int n) {
	
    int min = Integer.MAX_VALUE;
    
    int max = Integer.MIN_VALUE;
    
    System.out.println(min + " " + max);

    for (int i = 0; i < n; i++) {
        if (arr[i] < min) {
            min = arr[i];
        }
        if (arr[i] > max) {
            max = arr[i];
        }
    }

    System.out.println("Minimum element: " + min);
    System.out.println("Maximum element: " + max);
}



public static String reverseWord(String str)
{
   char[] arr =str.toCharArray() ; 
   
   int j =0 ;
   
   char[] nar = new char [str.length()];
   
   for(int i = arr.length -1 ; i >=0 ; i--){
       
      nar[j] =  arr[i]  ;
      
      j++ ; 
   }
   
   String k = Arrays.toString(nar) ;
   
   return k ;
   // Reverse the string str
}


public static void sort(int[] arr , int n) {
	
    int temp = 0 ;
    
    for(int i =0 ; i < n ; i++){
        
        for(int j =i+1 ; j <n ; j++){
           
           if(arr[i] > arr[j]){
               
              temp = arr[i] ;

               arr[i] = arr[j] ;
           
               arr[j] = temp ;
           }
           

        }
         
          
         // arr[i] ;
    } 
    
    
    for(int i =0 ; i < n ; i++) {
    	
    	 System.out.println(arr[i]);
    }
    // code here
    
       //  return arr[j] ;

	
	
}


 public static void rearrange(int arr[], int n) {
	
    int j =0 , temp ;
    
    for(int i= 0 ; i < n ; i++){
        
        if(arr[i] >= 0){   
            
            if(i != j){
                
                temp = arr[i] ;
                
                arr[i] = arr[j] ;
                
                arr[j] = temp ;
            }
            j++ ;

        }
        
      
        
    }        
    
}  // Your code goes here


// This method print the how many duplicacates in element  array 
public static int firstRepeating(int arr[], int n) {
	
    HashMap<Integer, Integer> indexMap = new HashMap<>();
    
    int minIndex = Integer.MAX_VALUE;

    // Traverse the array from left to right
    for (int i = 0; i < n; i++) {
        if (indexMap.containsKey(arr[i])) {
// Update minIndex if the current element is repeating and its index is smaller
            minIndex = Math.min(minIndex, indexMap.get(arr[i]));
        } else {
            // Store the index of the first occurrence of each element
            indexMap.put(arr[i], i + 1);
        }
    }

    // If minIndex remains MAX_VALUE, no repeating element found
    if (minIndex == Integer.MAX_VALUE)
        return -1;
    else
        return minIndex; // Return the 1-based index of the first repeating element
}




public static void rotateByOne(int[] arr, int n) {
	
	 if(n <= 1)
		    System.out.print(arr[1]);

		        
		        int last = arr[n-1] ;
		        
		        for(int i = n - 1 ; i > 0 ; i--){
		            
		            arr[i] = arr[i - 1] ;
		        }
		        
		        arr[0] = last ;
		            
    for (int num : arr) {
        System.out.print(num + " ");
       }

}



public static int firstNonRepeating(int arr[], int n) {
	
 ///   HashMap<Integer, Integer> frequencyMap = new HashMap<>();
    
    HashMap<Integer,Integer> ll = new HashMap<>() ;
    
    for(int i =0 ; i < n ; i++){
        
     ll.put(arr[i],ll.getOrDefault(arr[i],0)+1) ;
        
    }
    
    for(int i =0 ; i < n ; i++){
        
        if (ll.get(arr[i]) == 1){
            
            return arr[i] ;
        }
    }
    

     return 0 ;   
}


	public static void main(String[] args) {
		
		
	//	romanToInt("III");  
	
	// convertToRoman(3794);
	
//	  String[] S = { "the", "quick", "brown", "fox", 
//			     "quick"} ;
//	  String  word1 = "the" ;
//	  String  word2 = "fox" ;
//          
//	           
//	 minDistance(S,word1,word2);
	
		
		// peak element 
		
		 
//		int[] arr1 = {1,2,3,4,7,8,9};
//		       int n1 = arr1.length;
//		       findPeakElement(arr1, n1);
		       
		 
           

        //		
	      //  findPeakElement(arr1,n1) ;
	
//   find Min / Max Value :
        
//        int[] arr = {3, 2, 1, 56, 10000, 167};
//        int n = arr.length;
//       findMinMax(arr, n);
		
//		
		
//		String v=reverseWord("Geeks") ;
//		
//	    
//		   System.out.println(v);
		
//		 int[] arr = {3, 2, 1, 56, 10000, 167};
//           int n = arr.length;
//           
//		  sort(arr, n);
//
		
		// arrange negative and positive
		
		
//		 int arr1[] = {1, -1, 3, 2, -7, -5, 11, 6};
//	        int n1 = arr1.length;
//	        rearrange(arr1, n1);
	        
//	        System.out.println("Output for Example 1:");
//	        for (int i = 0; i < n1; i++)
//	            System.out.print(arr1[i] + " ");
//	        System.out.println();
//	
//		
		// cyclic rotate Array 
		
//         int[] arr1 = {1, 2, 3, 4, 5};
//         int n1 = arr1.length;
//          System.out.println("Output for Example 1:");
//        rotateByOne(arr1, n1);
		///   System.out.println();

	
		
		// find repeating elemrnt
        
		
//		int arr1[] = {1,2,3,4,5,6,7,8,8};
//         int n1 = arr1.length;
//       System.out.println("Output for Example 1:");
    //     System.out.println(firstRepeating(arr1, n1));
        
//	

	// print non repeating element

//	
       int arr1[] = { 1, 2, 2};
        int n1 = arr1.length;
////       System.out.println("Output for Example 1:");
      int n = firstNonRepeating(arr1, n1);
//   
//
////       
       System.out.println(n);
//
//////	
////	
////	
//	
//	
	
	
	
	
	
	
	
	}
	
	
	
	
}

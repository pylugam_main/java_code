package programjava;

import java.util.HashSet;
import java.util.Set;
// write program long array conert int array
public class StringHarder3 {
	
public static void findCommonElements(int[] A, int[] B, int[] C) {
	
	        int n1 = A.length;
	        int n2 = B.length;
	        int n3 = C.length;
	        
	        int i = 0, j = 0, k = 0;
	        

//	        int[] A = {1, 5, 10, 20, 40, 80};
//	        int[] B = {6, 7, 20, 80, 100};
//	        int[] C = {3, 4, 15, 20, 30, 70, 80, 120};
	     // try for loop 

	        
	        
	        // Traverse through all three arrays
	        while (i < n1 && j < n2 && k < n3) {
	            // If the current elements in all three arrays are equal, print it
	            if (A[i] == B[j] && B[j] == C[k]) {
	                System.out.print(B[j] + " ");
	                i++;
	                j++;
	                k++;
	            } 
	            // If the current element in A is smaller than the current 
	           //  element in B, move to the next element in A
	            
	            else if (A[i] < B[j]) {
	                i++;
	            } 
	            // If the current element in B is smaller than the 
	            //current element in C, move to the next element in B
	            else if (B[j] < C[k]) {
	                j++;
	            } 
	            // If none of the above conditions are true, move to the 
	            //next element in C
	            else {
	                k++;
	            }
	        }
	    }

public static int findMissingElement(int[] A, int N) {
	
    int total = (N * (N + 1)) / 2; // Calculate the sum of first N natural numbers
    int sum = 0;
    
    // Calculate the sum of elements in the array
    for (int num : A) {
        sum += num;
    }
    
    // The missing element is the difference between the total 
    // sum and the sum of elements in the array
    return total - sum;
}


 public static boolean hasZeroSumSubarray(int[] arr) {
	
    Set<Integer> set = new HashSet<>();
    int prefixSum = 0;

    // Traverse the array
    for (int num : arr) {
        prefixSum += num;

        // If prefix sum is 0 or prefix sum is seen before, return true
        if (prefixSum == 0 || set.contains(prefixSum))
            return true;

        // Add prefix sum to the set
        set.add(prefixSum);
    }

    // If no subarray with sum 0 is found, return false
    return false;		
}


public static int maxSubarraySum(int[] arr) {
	
    int maxSum = arr[0]; // Initialize maxSum with the
//    first element of the array
    int currentSum = arr[0]; // Initialize currentSum with the 
//    first element of the array

    // Traverse the array from the second element
    for (int i = 1; i < arr.length; i++) {
        // Update currentSum either by adding the current 
//    	element or starting a new subarray
        currentSum = Math.max(arr[i], currentSum + arr[i]);
        // Update maxSum if currentSum is greater
        maxSum = Math.max(maxSum, currentSum);
    }
    return currentSum;
}


public static int convertStringToInt(String s) {
	
    if (s == null || s.isEmpty()) {
        return -1;
    }

    int result = 0;
    int startIndex = 0;
    boolean isNegative = false;

    if (s.charAt(0) == '-') {
        isNegative = true;
        startIndex = 1;
    }

    for (int i = startIndex; i < s.length(); i++) {
        char c = s.charAt(i);
        if (c < '0' || c > '9') {
            return -1;
        }
        result = result * 10 + (c - '0');
    }

    return isNegative ? -result : result;
}


 public static String multiplyStrings(String s1, String s2) {
	
	
    if (s1 == null || s2 == null || s1.isEmpty() || s2.isEmpty()) {
        return "0";
    }

    int sign = 1;
    if (s1.charAt(0) == '-') {
        sign *= -1;
        s1 = s1.substring(1);
    }
    if (s2.charAt(0) == '-') {
        sign *= -1;
        s2 =  s2.substring(1);
    }

    int[] result = new int[s1.length() + s2.length()];

    for (int i = s1.length() - 1; i >= 0; i--) {
        int digit1 = s1.charAt(i) - '0';
        for (int j = s2.length() - 1; j >= 0; j--) {
            int digit2 = s2.charAt(j) - '0';
            int mul = digit1 * digit2;
            int sum = mul + result[i + j + 1];
            result[i + j + 1] = sum % 10;
            result[i + j] += sum / 10;
        }
    }

    StringBuilder sb = new StringBuilder();
    for (int num : result) {
        if (!(sb.length() == 0 && num == 0)) { // Skip leading zeros
            sb.append(num);
        }
    }

    if (sb.length() == 0) {
        return "0";
    }

    if (sign == -1) {
        sb.insert(0, '-');
    }

    return sb.toString();
}


public static void main(String[] args) {
	
	
//	
	        int[] A = {1, 5, 10, 20, 40, 80};
	        int[] B = {6, 7, 20, 80, 100};
	        int[] C = {3, 4, 15, 20, 30, 70, 80, 120};
//	        
	     //   System.out.print("Output: ");
//	        findCommonElements(A, B, C);
//	
	
// missing element
	
    int N1 = 5;
    int[] A1 = {1, 2, 3, 5};
 System.out.println(findMissingElement(A1, N1));
   
//    
//    int N2 = 10;
//    int[] A2 = {6, 1, 2, 8, 3, 4, 7, 10, 5};
//    System.out.println("Output for N = " + N2 + ": " + findMissingElement(A2, N2));       
//	        
	        
	 // sub Array with 0 sum
	
//    int[] arr1 = {2, -1, 4, 1, 2};
//  int[] arr2 = {4, 2, 0, 1, 6};
//     System.out.println(hasZeroSumSubarray(arr1)); // Output: true
//   System.out.println(hasZeroSumSubarray(arr2)); // Output: true
//

         // array sum max 9
	
    int[] arr1 = {1, 2, 3, -2, 5};
//    int[] arr2 = {-1, -2, -3, -4};
   System.out.println(maxSubarraySum(arr1)); // Output: 9
//    System.out.println(maxSubarraySum(arr2)); // Output: -1
   
   
   
   
   // String Integer To normal integer
   
//    String s1 = "-312";
//   String s2 = "21a";
//
//   int result1 = convertStringToInt(s1);
//   int result2 = convertStringToInt(s2);
//
//   System.out.println("Input: " + s1);
//    System.out.println( result1);
//
//   System.out.println("Input: " + s2);
//   System.out.println("Output: " + result2);
//   
	
	
	// String Multiplivcation 
//	
      String s1 = "0033";
      String s2 = "-2";
//////
//       String result = multiplyStrings(s1, s2);
//////
////      System.out.println("Input s1: " + s1);
////      System.out.println("Input s2: " + s2);
//        System.out.println(result);
//      
//	
	
	
	
}	
	

}

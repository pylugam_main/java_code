package programjava;

public class MCQProgram {

	private int x,y ;

	private double re,im ;
	
public static void condition() {
	
	// throws copilition error
	
	if(true) {
		
	//	break ;
	}
	
	int $ = 10 ;
	
	System.out.println('j' + 'a' + 'v' + 'a');
	
}	

public static void objectcheck() {
	
	Integer num1 = 100 ;
	
	Integer num2 = 100 ;
	
	Integer num3 = 1000 ;
	
	Integer num4 = 1000 ;
	
	if(num1 == num2)
		System.out.println("num1 == num2");
	else
		System.out.println("num3 != num4");
}

public static void test() {
	
	//  error unsolved compilition problem 
	
	for(int i =0 ; /*1*/ ; i++) {
		
		System.out.println("hi");
		break ;
	}
	
	
}

int fun() {
	
	//  compile time error
	
	return 20 ;
}

static int fun1() {
	
	// throws compile time error
	
	return 20 ;
}

public static int fun2() {
	
	// compilition error
	
	static int x = 0 ;
	
	return ++x ;
	
	
}

public static void checkvalue() {
	
	// compile time error
	
	 int y = 0 8 ;
	 
	 System.out.println(y);
	
}

public static void checkstring() {
	
	String s1 = "abc" ;
	
	String s2 = s1 ;
	
	     s2 += "d" ;
	
	System.out.println(s1 + " " + s2 + " " + (s1 == s2));
	
	StringBuffer ll = new StringBuffer("abc") ;
	
	StringBuffer  kk = ll ;
	
	ll.append("d") ;
	
	System.out.println(ll + " " + kk + (ll == kk));
	
}

public static void gfg(String s) {
	
	System.out.println("string");
}

public static void gfg(Object o) {
	
	System.out.println("Object");
}

public String toString() {
	
	return "c"+ re + "*" + im + "i" ;
}

public MCQProgram(MCQProgram s) {
	
	re = s.re ;
	
	im = s.im ;
	// TODO Auto-generated constructor stub
}

public static void longnum() {
	
	// compilition error
	
	long num = 1000000000000 ;
	
	System.out.println(num);
	
}

public static void longnum1() {
	
	// compilition error
	
	long num = 10000 * 10000 * 10000 * 10000 ;
	
	System.out.println(num);
	
}

static int x1 = 1111 ;

static {
	
	x1 = x1-- +--x1 ;
} 

public static void floattest() {
	
	Double mynum = 2.123456789123456789 ;
	
	float d = mynum.floatValue() ;
	
	double c = mynum.doubleValue() ;
	
	System.out.println(d);
	
	System.out.println(c);
	
}


public static void stringtest2() {
	
	System.out.println("MOON" + 10 + 20);
}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
           
			condition() ;
		
		    objectcheck();
		    
//		    MCQProgram ll = new MCQProgram() ;
	    
//		    System.out.println(ll.x + " " + ll.y);
		    
		    test();
		    
		 //   System.out.println(fun());
	
		    System.out.println(fun1());
		    
		 //   System.out.println(fun2());
		   
		  //  checkvalue();
		    
		    checkstring();
		    
		    gfg(null);
		    
		//  MCQProgram l1 = new MCQProgram() ;
		  
		//  MCQProgram l2 = new MCQProgram(l1) ;
		  
		//  System.out.println(l2);
		    
		 //   longnum();
		    
		    longnum1();
	
		    System.out.println(x1);
		    
		    floattest();
		    
		    stringtest2();
	}

public static void main(String args) {
		
	
	}	
	
}

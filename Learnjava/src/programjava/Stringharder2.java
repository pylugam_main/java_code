package programjava;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class Stringharder2 {

 public static void minimumDeletions(String S) {
	
	        int n = S.length();
	        int[][] dp = new int[n][n];

	        for (int l = 2; l <= n; l++) {
	            for (int i = 0; i < n -l + 1; i++) {
	                int j = i + l - 1;
	                if (S.charAt(i) == S.charAt(j))
	                    dp[i][j] = dp[i + 1][j - 1];
	                else
	                    dp[i][j] = 1 + Math.min(dp[i][j - 1], dp[i + 1][j]);
	            }
	        }
	        System.out.println( dp[0][n - 1]);
	    }	
	


public static int findEqualPoint(String str) {
	
    int openCount = 0;
    int closeCount = 0;

    for (int i = 0; i < str.length(); i++) {
        if (str.charAt(i) == '(') {
            openCount++;
        } else {
            closeCount++;
        }

        if (openCount == closeCount) {
            return i + 1;
        }
    }

    return -1; 
    
    // No equal point found
}

public static void countbracket() {
	
	
	   String str = "(())))(";
	   
	int len = str.length(); 
	int open[] = new int[len+1]; 
	int close[] = new int[len+1]; 
	int index = -1; 

	open[0] = 0; 
	close[len] = 0; 
	if (str.charAt(0)=='(') 
		open[1] = 1; 
	if (str.charAt(len-1) == ')') 
		close[len-1] = 1; 

	// Store the number of opening brackets 
	// at each index 
	for (int i = 1; i < len; i++) 
	{ 
		if ( str.charAt(i) == '(' ) 
			open[i+1] = open[i] + 1; 
		else
			open[i+1] = open[i]; 
	} 

	// Store the number of closing brackets 
	// at each index 
	for (int i = len-2; i >= 0; i--) 
	{ 
		if ( str.charAt(i) == ')' ) 
			close[i] = close[i+1] + 1; 
		else
			close[i] = close[i+1]; 
	} 

	// check if there is no opening or closing 
	// brackets 
	if (open[len] == 0) 
		System.out.println(len);
	if (close[0] == 0) 
		System.out.println(0); 

	// check if there is any index at which 
	// both brackets are equal 
	for (int i=0; i<=len; i++) 
		if (open[i] == close[i]) 
			index = i; 

	System.out.println(index);
	
	
	
}
public static int countDistinctSubsequences(String s) {
	
    int n = s.length();
    int mod = 1000000007;
    int[] dp = new int[n + 1];
    int[] last = new int[26];
    Arrays.fill(last, -1);
    dp[0] = 1;

    for (int i = 1; i <= n; i++) {
    	
       int d = (2 * dp[i - 1]) % mod;

  // 	int c = dp[last[s.charAt(i - 1) - 'a']] ;

       
        dp[i] = (2 * dp[i - 1]) % mod;
        if (last[s.charAt(i - 1) - 'a'] != -1) // a ascii value 141
        	dp[i] = (dp[i] - dp[last[s.charAt(i - 1) - 'a']] + mod) % mod;
                   
        last[s.charAt(i - 1) - 'a'] = i - 1;
    }
    return dp[n];
}

public static int smallestMissingPositive(int[] arr) {
	
    int n = arr.length;

    // Separate positive and negative numbers
    int shift = segregate(arr, n);
    int[] arr2 = new int[n - shift];
    int j = 0;
    for (int i = shift; i < n; i++) {
        arr2[j] = arr[i];
        j++;
    }

    // Mark arr2[i] as visited by making arr2[arr2[i] - 1] negative.
   // Note that 1 is subtracted because indexing starts from 
    // 0 and positive numbers start from 1.
   
    for (int i = 0; i < arr2.length; i++) {
        int x = Math.abs(arr2[i]);
        if (x - 1 < arr2.length && arr2[x - 1] > 0) {
            arr2[x - 1] = -arr2[x - 1];
        }
    }

    // Return the first index value at which arr2[i] is positive
    for (int i = 0; i < arr2.length; i++) {
        if (arr2[i] > 0) {
            return i + 1;
        }
    }

    // If no positive value is found, return n+1
    return arr2.length + 1;
}

public static int segregate(int[] arr, int n) {
    int j = 0;
    for (int i = 0; i < n; i++) {
        if (arr[i] <= 0) {
            int temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
            j++;
        }
    }
    return j;
}


 public static boolean isSubset(long[] a1, long[] a2) {
    // Create a HashSet to store elements of a1
    HashSet<Long> set = new HashSet<>();
    
    // Add all elements of a1 to the set
    for (long num : a1) {
        set.add(num);
    }
    
    // Check if all elements of a2 are present in the set
    for (long num : a2) {
        if (!set.contains(num)) {
            return false;
        }
    }
    
    return true;
}


public static List<Integer> factorialDigits(int N) {
	
    List<Integer> result = new ArrayList<>();
    
    // Initialize an array to store the factorial digits
    int[] factorial = new int[10000];
    factorial[0] = 1;
    int size = 1;
    
    // Calculate the factorial
    for (int i = 2; i <= N; i++) {
        size = multiply(i, factorial, size);
    }
    
    // Convert the factorial to a list of digits
    for (int i = size - 1; i >= 0; i--) {
        result.add(factorial[i]);
    }
    
    return result;
}

// Function to multiply a number represented as an array with an integer
public static int multiply(int x, int[] res, int size) {
    int carry = 0;
    
    // Multiply each digit of res with x and update the array
    for (int i = 0; i < size; i++) {
        int prod = res[i] * x + carry;
        res[i] = prod % 10;
        carry = prod / 10;
    }
    
    // Add remaining carry to the result
    while (carry != 0) {
        res[size] = carry % 10;
        carry = carry / 10;
        size++;
    }
    
    return size;
}

public static void subsetloop() {
	
	int arr1[] = { 1 ,2 ,3 ,4, 5 ,6 ,7 ,8 } ;

    int arr2[] = {1 ,2 ,3 ,1};

    int m = arr1.length ;
    int n = arr2.length	;
	
	 int i = 0;
     int j = 0;
     for (i = 0; i < n; i++) {
         for (j = 0; j < m; j++)
             if (arr2[i] == arr1[j])
                 break;

         /* If the above inner loop
         was not broken at all then
         arr2[i] is not present in
         arr1[] */
         if (j == m)
     System.out.println("No ");
     }

     /* If we reach here then all
     elements of arr2[] are present
     in arr1[] */
     System.out.println("Yes");
}



public static void main(String[] args) {
		// TODO Auto-generated method stub
	
	
//	   subsetloop();

//		String S = "abefbac" ;
//
//       minimumDeletions(S);
//        
   
    //   String str1 = "(())))(";
      //  String str2 = "))";
    //   System.out.println(findEqualPoint(str1)); // Output: 4
		
         String s = "gfg";
         System.out.println(countDistinctSubsequences(s));
  
	
//	countbracket();


	// Missing Integer
	
	
	  int[] arr1 = {2,3,-6,-7,4};
     // int[] arr2 = {1,2,3,4,5};
//
      //System.out.println("Output for arr1: " + smallestMissingPositive(arr1));
   System.out.println(smallestMissingPositive(arr1));
	

// subset Array
//	
//       long[] a1 = {11, 7, 1, 13, 21, 3, 7, 3};
//       long[] a2 = {11, 3, 7, 1, 7,7};
//    
//      if (isSubset(a1, a2)) {
//        System.out.println("Yes");
//        } else {
//        System.out.println("No");
//         }
    
//    long[] a3 = {1, 2, 3, 4, 4, 5, 6};
//    long[] a4 = {1, 2, 4};
//    
//    if (isSubset(a3, a4)) {
//        System.out.println("Yes");
//    } else {
//        System.out.println("No");
//    }
//

	
	// factorial progeam
	
//    int N = 5;
//   List<Integer> result1 = factorialDigits(N);
////  //  System.out.print( N );
//    for (int digit : result1) {
//        System.out.print(digit);
//    }
//    System.out.println();
////    
//    N = 10;
//    List<Integer> result2 = factorialDigits(N);
//    System.out.print("Output for N = " + N + ": ");
//    for (int digit : result2) {
//        System.out.print(digit);
//    }
//    System.out.println();

}

}

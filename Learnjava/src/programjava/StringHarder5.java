package programjava;

import java.util.ArrayList;
import java.util.List;

public class StringHarder5 {
	
	 
public static int minDistance(String word1, String word2) {
	
        int m = word1.length();
        int n = word2.length();
        
        // Create a 2D array to store the minimum steps required to make substrings the same
        int[][] dp = new int[m + 1][n + 1];
        
        // Initialize the first row and column
        for (int i = 0; i <= m; i++) {
            dp[i][0] = i;
        }
        for (int j = 0; j <= n; j++) {
            dp[0][j] = j;
        }
        
        // Fill the dp array
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                if (word1.charAt(i - 1) == word2.charAt(j - 1)) {
                    dp[i][j] = dp[i - 1][j - 1];
                } else {
                    dp[i][j] = 1 + Math.min(dp[i - 1][j], dp[i][j - 1]);
                }
            }
        }
        
        // Return the minimum steps required to
        // make the entire strings the same
        return dp[m][n];
    }
    
    
public static  boolean isNumber(String s) {
	
	
if (s == null || s.trim().isEmpty()) {
  return false;
}

// Remove leading and trailing spaces
s = s.trim();

boolean numberSeen = false;
boolean pointSeen = false;
boolean eSeen = false;
boolean numberAfterE = true;

for (int i = 0; i < s.length(); i++) {
  char c = s.charAt(i);
  
  if (Character.isDigit(c)) {
      numberSeen = true;
      numberAfterE = true;
  } else if (c == '.') {
      if (pointSeen || eSeen) {  // if only is true excutes loop
          return false;
      }
      pointSeen = true;
  } else if (c == 'e' || c == 'E') {
      if (eSeen || !numberSeen) {       // ! symbol digit boolean true 
    	                                //thay's why used
          return false;
      }
      eSeen = true;
      numberAfterE = false;
  } else if (c == '+' || c == '-') {
      if (i != 0 && s.charAt(i - 1) != 'e' && s.charAt(i - 1) != 'E') {
          return false;
      }
  } else {
      return false;
  }
}

return numberSeen && numberAfterE;

}



private void permute(String str, int l, int r) 
{ 
    if (l == r) 
        System.out.println(str); 
    else { 
        for (int i = l; i <= r; i++) { 
            str = swap(str, l, i); 
            permute(str, l + 1, r); 
            str = swap(str, l, i); 
        } 
    } 
} 

/** 
 * Swap Characters at position 
 * @param a string value 
 * @param i position 1 
 * @param j position 2 
 * @return swapped string 
 */
public String swap(String a, int i, int j) 
{ 
    char temp; 
    char[] charArray = a.toCharArray(); 
    temp = charArray[i]; 
    charArray[i] = charArray[j]; 
    charArray[j] = temp; 
    return String.valueOf(charArray); 
}

static void printPatternUtil(String str, char buf[],
        int i, int j, int n)
{
if (i == n) 
{
buf[j] = '\0';
System.out.println(buf);
return;
}

// Either put the character
buf[j] = str.charAt(i);
printPatternUtil(str, buf, i + 1, 
      j + 1, n);

// Or put a space followed 
// by next character
buf[j] = ' ';
buf[j + 1] = str.charAt(i);

printPatternUtil(str, buf, i + 1, 
     j + 2, n);
}

// Function creates buf[] to 
// store individual output
// string and uses printPatternUtil() 
// to print all
// permutations
static void printPattern(String str)
{
int len = str.length();

// Buffer to hold the string 
// containing spaces
// 2n-1 characters and 1 
// string terminator
char[] buf = new char[2 * len];

// Copy the first character as it is, since it will
// be always at first position
buf[0] = str.charAt(0);
printPatternUtil(str, buf, 1, 1, len);
}



public static void calcSubset(List<Integer> A,
        List<List<Integer> > res,
        List<Integer> subset,
        int index)
{
// Add the current subset to the result list
res.add(new ArrayList<>(subset));

// Generate subsets by recursively including and
// excluding elements
for (int i = index; i < A.size(); i++) {
// Include the current element in the subset
subset.add(A.get(i));

// Recursively generate subsets with the current
// element included
calcSubset(A, res, subset, i + 1);

// Exclude the current element from the subset
// (backtracking)
subset.remove(subset.size() - 1);
}
}

public static List<List<Integer> >
subsets(List<Integer> A)
{
List<Integer> subset = new ArrayList<>();
List<List<Integer> > res = new ArrayList<>();

int index = 0;
calcSubset(A, res, subset, index);

return res;

}


public static void main(String[] args) {
	
	// print array subset program 

//	 List<Integer> array = List.of(1, 2, 3);
//     List<List<Integer> > res = subsets(array);

    //   Print the generated subsets
//     for (List<Integer> subset : res) {
//         for (Integer num : subset) {
//             System.out.print(num + " ");
//         }
//         System.out.println();
  //   }


	
	
//	ABC space program
	
//   	 String str = "ABC";
//     printPattern(str);
	
	
	///String permutations
	
//	 String str = "ABC"; 
//     int n = str.length(); 
//     StringHarder5 permutation = new StringHarder5(); 
//     permutation.permute(str, 0, n - 1); 
     
     
//		System.out.println("Hello World");
		
	String word1 = "sea" ; 
	 
	String  word2 = "eat" ;
	
  //    System.out.println(minDistance(word1, word2));
		
             String s = "-123.456e789" ;
              System.out.println(isNumber(s));

	}
	


}

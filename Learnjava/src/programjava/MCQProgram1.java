package programjava;

public class MCQProgram1 {

	public static void stringtest3() {
		
		System.out.println(1 + 2 + "MOON");
	}

	public static void stringtest4() {
		
		// new key word used to new object 
		
		String s1 = "MOON";
		
		String s2 = new String("MOON") ;
				
		System.out.println(s1 == s2);
		
	}

	public static void stringtest5() {
		
		// compile time error
		
		final String a ;
		
		a = "MOON" ;
		
		a = "YOUTUBE" ;
		
		System.out.println(a);

	}

	public static void stringtest6() {
		
		MCQProgram1 obj = new MCQProgram1() ;
		
		System.out.println(obj instanceof MCQProgram1);
		
	}

	public static void stringtest7() {
		
	// comple time error	
		
		if(1) {
			System.out.println("hellow1");
		}
		else {
			
			System.out.println("bye");
		}

	}

	public static void conditin2() {
		
		int score = 180 ;
		
		String grade ;
		
		if(score >= 90)
	      grade = "A"	 ;
		
		if(score >= 80)
			grade = "B" ;
		
		if(score >= 70)
		  grade = "C" ;
		
		if(score >= 60)
			grade = "D" ;
		else
			grade = "E" ;
		
		System.out.println(grade);
	}

	public static void unary() {
		
		int x = 1 ;
		
		System.out.println(x--);
		
		
	}

	public static void unary1() {
		
	// compile time error	
		
		int x = 1 ;
		
		System.out.println(--x--);
	}

	public static void unary2() {
		
		// compile time error
		
		int x,y,z ;
		
		x = 4 ; y = 8 ;
		
	 	z *= --y - y-- + ++y + x++ - ++x ;
	 	
	 	System.out.println(x + " " + y);
	 	
	}

	public static void looptest() {
		
		// compile time error
		
		switch(int n = 1 ) {
		
		case 1 : System.out.println("moon");
		}
		
	}

	public static void looptest1() {
		
		// compile time error
		
		int b = 8 ;
		
		for(int a = 1 ; a <= a^b>2 ; a++) 
			
			if(!(b == 4))
				System.out.println(b);
			
			System.out.println(a + " " + b);
		
	}

	public static void stringtest2() {
		
		String sb = new String("land");
		
	sb.concat("mister").replace('d', 'n').toUpperCase().replace('M', 'N').toString() ;

	     System.out.println(sb);

	}
	
public static void looptest2() {
	
    
//			int a = Integer.parseInt(args[0]) ;
//			
//			int b = Integer.parseInt(args[1]) ;
//			
//			switch(a > b) {
//			
//			case true :
//				System.out.println("A is greater than B");
//				
//			case false :
//				System.out.println("B is greater than A");
//			
//			}

}

public static void stringtest8() {
	
	String s1 = new String("MOON");
	
	String s2 = new String("youtube");
	
	System.out.println(s1 = s2);
} 

public static void exceptiontest() {
	
	// compilitin error
	
	try {
		
		int a = 10 ;
		
		int b = 5 ;
		
		int c = a / b ;
		
		System.out.println("hellow");
	}

}

public static void exceptiontest2() {
	
	try {
		
		int x = 0 ;
		
		int y = 5 ;
		
		int z = y / x ;
		
		System.out.println("are");
	}
	catch (Exception e) {
		// TODO: handle exception
		
		System.out.println("how");
	}
	finally {
		System.out.println("are");
	}
}

public static void arrtest() {
	
	// Arithmetic exception
	
	int[] arr = new int[] {1,2,3,4};
	
	int x = arr[0]-- + arr[2]-- / arr[0] * arr[5] ;
	
	System.out.println(x);
	
}

public static void condition3() {
	
	if(77 == 7_7) {
		System.out.println("moon");
	}
	else {
		System.out.println("no moon");
	}
	
}

void MCQProgram1() {
	
	System.out.println("class A");
}

public static void looptest4() {
	
	int arr[] = {1,2,3,4,5};
	
	for(int i = 0 ; i < arr.length-2 ; ++i) {
		
		System.out.println(arr[i] + "");
		
	}
}

public static void unarytest() {
	
// compile time error	
	
	int a = 5 ;
	
     a =(++a)++ ;
 
 System.out.println(a);
}

public static void conditiontest() {
	
	int a = 3 ;
	
	if(a == 3) {
		
		int b = 5 ;
		
		System.out.println("sum =" + (a+b));
	}
}
		public static void main(String[] args) {
			// TODO Auto-generated method stub
	        			
			new MCQProgram1() ;
			
			looptest4();
			
		//	stringtest3();
			
		//	stringtest4();
			
		//	stringtest5();
			
		//	stringtest6();
			
		//	stringtest7();
			
		//	conditin2();
			
		//	unary();
			
		//	unary1();
			
		//	unary2();
			
		//	looptest();
			
		//looptest1();
			
//	     stringtest2();
//	     
//	     looptest2();
//		
//	     stringtest8();
//	     
//	     exceptiontest();
//		
//	     exceptiontest2();
	     
	  //   arrtest();
			
	//	condition3();
		
	  //   unarytest();
			
	conditiontest();		
		}

	
}

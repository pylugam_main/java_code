package programjava;

import java.util.ArrayList;

public class StringHarder4 {
	
	public static int longestPalindromeSubseq(String s) {
		
	    int n = s.length();
	    int[][] dp = new int[n][n];

	    // Strings of length 1 are palindromes of length 1
	    for (int i = 0; i < n; i++) {
	        dp[i][i] = 1;
	    }

	    // Bottom-up dynamic programming
	    	for (int len = 2; len <= n; len++) {
	        for (int start = 0; start <= n - len; start++) {
	            int end = start + len - 1;
	            if (s.charAt(start) == s.charAt(end) && len == 2) {
	                dp[start][end] = 2;
	            } else if (s.charAt(start) == s.charAt(end)) {
	                dp[start][end] = dp[start + 1][end - 1] + 2;
	            } else {
	                dp[start][end] = Math.max(dp[start + 1][end], dp[start][end - 1]);
	            }
	        }
	    }

	    return dp[0][n - 1];
	}


	static long getSubstringWithEqual012(String s)
	{

		ArrayList<String> arr = new ArrayList<>();
		int n = s.length();
		// generating subarrays
		for (int i = 0; i < n; i++) {
			for (int j = i; j < n; j++) {
				String s1 = "";
				for (int k = i; k <= j; k++) {
					s1 += s.charAt(k);
				}
				arr.add(s1);
			}
		}
		int count = 0;
		int countZero, countOnes, countTwo;
		// iterating over array of all substrings
		for (int i = 0; i < arr.size(); i++) {
			countZero = 0;
			countOnes = 0;
			countTwo = 0;
			String curs = arr.get(i);
			for (int j = 0; j < curs.length(); j++) {
				if (curs.charAt(j) == '0')
					countZero++;
				if (curs.charAt(j) == '1')
					countOnes++;
				if (curs.charAt(j) == '2')
					countTwo++;
			}
			// if number of ones,two and zero are equal in a
			// substring
			if (countZero == countOnes
				&& countOnes == countTwo) {
				count++;
			}
		}

		return count;
	}
	
	
	
	public static String reformatString(String S, int K) {
		
        StringBuilder result = new StringBuilder();
        int count = 0;

        for (int i = S.length() - 1; i >= 0; i--) {
            char ch = S.charAt(i);
            if (ch != '-') {
                if (count == K) {
                    result.insert(0, '-');
                    count = 0;
                }
                result.insert(0, Character.toUpperCase(ch));
                count++;
            }
        }

        return result.toString();
    }

	
	
	public static int minRepeats(String A, String B) {
		
        int repeat = 1;
        StringBuilder sb = new StringBuilder(A);
        while (sb.length() < B.length()) {
            sb.append(A);
            repeat++;
        }
        if (sb.toString().contains(B)) {
            return repeat;
        }
        sb.append(A);
        if (sb.toString().contains(B)) {
            return repeat + 1;
        }
        return -1;
    }

	
public static void main(String[] args) {
	
	// longest Palindrome Sequence
	
   //  	 String s1 = "bbabcbcab";
          //String s2 = "abcd";

 //      int result1 = longestPalindromeSubseq(s1);
///     int result2 = longestPalindromeSubseq(s2);
//
//     System.out.println("Input: " + s1);
 //     System.out.println(result1);
//
//     System.out.println("Input: " + s2);
//     System.out.println("Output: " + result2);
//
	
	// String Equal 0,1,2
	
	String str = "0102010";
	System.out.println(getSubstringWithEqual012(str));

	

	
	// Licence key
	
	
//	 String S1 = "2-5g-3-J";
//     int K1 = 2;
//     String S2 = "2-5g-3-J";
//     int K2 = 2;
//
//     System.out.println("Input: " + S1 + ", " + K1);
 //     System.out.println(reformatString(S1, K1));
//
//     System.out.println("Input: " + S2 + ", " + K2);
//     System.out.println("Output: " + reformatString(S2, K2));	
	
	
// minimum A repated B must be substring
	
	 String A1 = "abcd";
     String B1 = "cdabcdab";
//     String A2 = "ab";
//     String B2 = "cab";
//
//     System.out.println("Input: A = \"" + A1 + "\", B = \"" + B1 + "\"");
 //    System.out.println( minRepeats(A1, B1));
//
//     System.out.println("Input: A = \"" + A2 + "\", B = \"" + B2 + "\"");
//     System.out.println("Output: " + minRepeats(A2, B2));
	
}
		
}

package programjava;

import java.util.Arrays;
import java.util.List;

public class StringHarder6 {

static void wordBreak(int n, List<String> dict, String s)
	  {
	    String ans="";
	    
	    wordBreakUtil(n, s, dict, ans);
	  }
	 
static void wordBreakUtil(int n, String s, List<String> dict, String ans)
	  {
	    for(int i = 1; i <= n; i++)
	    {
	 
	      // Extract substring from 0 to i in prefix
	      String prefix=s.substring(0, i);
	 
	      // If dictionary contains this prefix, then
	      // we check for remaining string. Otherwise
	      // we ignore this prefix (there is no else for
	      // this if) and try next
	      if(dict.contains(prefix))
	      {
	        // If no more elements are there, print it
	        if(i == n)
	        {
	 
	          // Add this element to previous prefix
	          ans += prefix;
	          System.out.println(ans);
	          return;
	        }
	        wordBreakUtil(n - i, s.substring(i,n), dict, ans+prefix+" ");
	      }
	    }
	  }
	


public static int findMinRec(int A[], int n) 
{ 
  // if size = 0 means whole array 
  // has been traversed 
  if(n == 1) 
    return A[0]; 
      int n1 =  Math.max(A[n-1], findMinRec(A, n-1)); 
    return n1 ;
} 

static void fibo(int n, int a, int b)
{
    if (n > 0) {
 
        // Function call
        fibo(n - 1, b, a + b);
 
        // Print the result
        System.out.println(a + " ");
    }
}

static void printNos(int n)
{
    if (n > 0) {
        printNos(n - 1);
        System.out.println(n + " ");
    }
    return;
}


static int factorial(int n) 
{ 
    if (n == 0) 
        return 1; 

    return n * factorial(n - 1); 
}


void reverse(String str)
{
    if ((str==null)||(str.length() <= 1))
       System.out.println(str);
    else
    {
        System.out.print(str.charAt(str.length()-1));
        reverse(str.substring(0,str.length()-1));
    }
}


public static int recurSum(int n) 
{ 
    if (n <= 1) 
        return n; 
    return n + recurSum(n - 1); 
}


static boolean isPalRec(String str, 
        int s, int e)
{
// If there is only one character
if (s == e)
return true;

// If first and last 
// characters do not match
if ((str.charAt(s)) != (str.charAt(e)))
return false;

// If there are more than 
// two characters, check if
// middle substring is also
// palindrome or not.
if (s < e + 1)
return isPalRec(str, s + 1, e - 1);

return true;
}

static boolean isPalindrome(String str)
{
int n = str.length();

// An empty string is 
// considered as palindrome
if (n == 0)
return true;

return isPalRec(str, 0, n - 1);
}


private static int recLen(String str) 
{

    // if we reachdk nkat the end of the string
	
    if (str.equals(""))
        return 0;
    else
        return recLen(str.substring(1)) + 1;
}


int catalan(int n)
{
    int res = 0;

    // Base case
    if (n <= 1) {
        return 1;
    }
    for (int i = 0; i < n; i++) {
        res += catalan(i) * catalan(n - i - 1);
    }
    return res;
}

static float findMean(int A[], int N) 
{ 
    if (N == 1) 
        return (float)A[N-1]; 
    else
        return ((float)(findMean(A, N-1)*(N-1) + 
                                    A[N-1]) / N); 
} 
	public static void main(String[] args) {
		
		// TODO Auto-generated method stub
		
		 String str1 = "iloveicecreamandmango"; // for first test case
		    String str2 ="ilovesamsungmobile";     // for second test case
		    int n1 = str1.length();                 // length of first string
		    int n2 = str2.length();                 // length of second string
//		 
//		    // List of strings in dictionary
		    List <String> dict= Arrays.asList("mobile","samsung","sam","sung",
		                                      "man","mango", "icecream","and",
		                                      "go","i","love","ice","cream");         
		    System.out.println("First Test:");
		 
//		    // call to the method
		    wordBreak(n1,dict,str1);
		    System.out.println("\nSecond Test:");
//		 
//		    // call to the method
		    wordBreak(n2,dict,str2);
		    
		    // fiboninancy
		    
//		    int N = 10;
//		    fibo(N, 0, 1);	    
		    
		    
		    // min and max
		
// 		    int A[] = {1, 4, 45, 6, -50, 10, 2}; 
//	        int n = A.length; 
//	          
//	        // Function calling 
//           System.out.println(findMinRec(A, n));    
//		    
//		    // factorial
//		    
//	        int num = 5; 
//	        System.out.println(factorial(5)); 

	        		
	        // print loops
	        
// 	        int n = 10;
//	        printNos(n);
  
	      // sum of natural
	        
//	        int n = 5; 
//	        System.out.println(recurSum(n)); 
	        
	     // reverse string
	        
// 	        String str = "Geeks for Geeks";
//	        StringHarder6 obj = new StringHarder6();
//	        obj.reverse(str);   
	        
	       // string palindrome
		
	 //       String str = "geeg";
	        
//	        if (isPalindrome(str))
//	            System.out.println("Yes");
//	        else
//	            System.out.println("No");
	    
	        // length of string
	        
	         String str ="GeeksforGeeks";
	        System.out.println(recLen(str)); 
	        
	       // mean Array
	        
	//        float Mean = 0; 
//	        int A[] = {1, 2, 3, 4, 5}; 
//	        int N = A.length; 
//	        System.out.println(findMean(A, N));        
	        
	        
	      //Program for nth Catalan Number
	        
//	        StringHarder6 cn = new StringHarder6();
//	        for (int i = 0; i < 10; i++) {
//	            System.out.print(cn.catalan(i) + " ");
//	        }     
	        
	}

}

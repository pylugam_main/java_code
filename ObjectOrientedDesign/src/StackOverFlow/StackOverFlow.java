package StackOverFlow;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class StackOverFlow {

	  private final Map<Integer, User> users;
	    private final Map<Integer, Questions> questions;
	    private final Map<Integer, Answer> answers;
	    private final Map<String, Tag> tags;

	    public StackOverFlow() {
	        users = new ConcurrentHashMap<>();
	        questions = new ConcurrentHashMap<>();
	        answers = new ConcurrentHashMap<>();
	        tags = new ConcurrentHashMap<>();
	    }

	    public User createUser(String username, String email) {
	        int id = users.size() + 1;
	        User user = new User(id, username, email);
	        users.put(id, user);
	        return user;
	    }

	    public Questions askQuestion(User user, String title, String content, List<String> tags) {
	        Questions question = user.askQuestion(title, content, tags);
	        questions.put(question.getId(), question);
	        for (Tag tag : question.getTags()) {
	            this.tags.putIfAbsent(tag.getName(), tag);
	        }
	        return question;
	    }

public Answer answerQuestion(User user, Questions question,
		           String content) {
	        Answer answer = user.answerQuestion(question, content);
	        answers.put(answer.getId(), answer);
	        return answer;
	    }

	    public Comment addComment(User user, Commmenentable commentable, String content) {
	        return user.addComment(commentable, content);
	    }

	    public void voteQuestion(User user, Questions question, int value) {
	        question.vote(user, value);
	    }

	    public void voteAnswer(User user, Answer answer, int value) {
	        answer.vote(user, value);
	    }

	    public void acceptAnswer(Answer answer) {
	        answer.markAsAccepted();
	    }

	    public List<Questions> searchQuestions(String query) {
	     return questions.values().stream()
	       .filter(q -> q.getTitle().toLowerCase().contains(query.toLowerCase()) ||
	      q.getContent().toLowerCase().contains(query.toLowerCase()) ||
	       q.getTags().stream().anyMatch(t -> t.getName().equalsIgnoreCase(query)))
	          .collect(Collectors.toList());
	    }

	    public List<Questions> getQuestionsByUser(User user) {
	        return user.getQuestions();
	    }

	    // Getters
	    public User getUser(int id) { return users.get(id); }
	    public Questions getQuestion(int id) { return questions.get(id); }
	    public Answer getAnswer(int id) { return answers.get(id); }
	    public Tag getTag(String name) { return tags.get(name); }
}

package callcenter;

import java.util.List;

public class CallHandler {

private final int Levels = 3 ;

private final int NUM_RESPONDENTS = 10 ;
private final int NUM_MANAGERS = 4 ;
private final int NUM_DIRECTORS = 2 ;

List<List<Employee>> employeeLevels ;

List<List<Call>> callQueues ;


public CallHandler() {
	// TODO Auto-generated constructor stub
}


public Employee geHandlerforCall(Call call) {
	
	return null;
}

public void dispatchCall(Caller caller) {
	
	Call call = new Call(caller);
	dispatchCall(call);
}

public void dispatchCall(Call call) {
	
	Employee emp = geHandlerforCall(call);
	
	if(emp != null) {
		emp.receiveCall(call);
		call.setHandler(emp);
	}
	else {
		call.reply("plese wait");
		callQueues.get(call.getRank().getValue()).add(call);
	}
}

public boolean assigncall(Employee emp) {
	return false;
	
}
	
	
}

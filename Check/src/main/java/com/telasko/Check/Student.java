package com.telasko.Check;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;

@Entity
public class Student {

	@Id
 private int rollname ;	
	
 private String name ;	
	
 private int marks ;
 
 //@ManyToMany(mappedBy = "stu")
   @OneToMany(mappedBy = "stu")
 private  List<Laptop> lap = new ArrayList<Laptop>() ;
 


public int getRollname() {
	return rollname;
}

public void setRollname(int rollname) {
	this.rollname = rollname;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public int getMarks() {
	return marks;
}

public void setMarks(int marks) {
	this.marks = marks;
}

public List<Laptop> getLap() {
	return lap;
}

public void setLap(List<Laptop> lap) {
	this.lap = lap;
}

@Override
public String toString() {
	return "Student [rollname=" + rollname + ", name=" + name + ", marks=" + marks + ", lap=" + lap + "]";
}	
	
	
	
}

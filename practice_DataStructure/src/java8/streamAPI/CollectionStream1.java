package java8.streamAPI;

import java.util.ArrayList;
import java.util.List;

public class CollectionStream1 {
	
public static void main(String[] args) {
	
	List<Integer> ll = new ArrayList<Integer>();
	
	ll.add(89);
	ll.add(88);
	ll.add(1);
	ll.add(55);
	
	
	ll.stream().sorted().forEach(System.out::println);
	
}


}

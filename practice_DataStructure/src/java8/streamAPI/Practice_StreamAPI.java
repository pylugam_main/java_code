package java8.streamAPI;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.IntStream;

public class Practice_StreamAPI {

public static void main(String[] args) {
	
  int[] num = {2,1,6,3,4,9,7,2,1,3,5,6,7};	
	
	       IntStream in =  Arrays.stream(num);
	
	       in = in.sorted();
	       
	in.forEach(System.out::println);
	
    System.out.println(  Arrays.stream(num).count());
	
      
	Arrays.stream(num).sorted().forEach(System.out::println);
	
 }	
	
}

package java8.doublecollen;

@FunctionalInterface
public interface Subject {

	public void mark(int value);
}

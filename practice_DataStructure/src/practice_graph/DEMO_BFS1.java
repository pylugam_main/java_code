package practice_graph;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class DEMO_BFS1 {

private LinkedList<Integer> adj[] ;	
	
private int V ;	
	
private int E ;	


public DEMO_BFS1(int nodes) {
	// TODO Auto-generated constructor stub

	this.V = nodes ;
	
	this.E = 0 ;
	
    this.adj = new LinkedList[nodes];

    for(int v= 0 ; v < V ; v++) {
    	
    	adj[v] = new LinkedList<Integer>();
    }
    
}	
	
public void addEdge(int u , int v) {
	
	adj[u].add(v) ;
	
	adj[v].add(u) ;
	
	E++ ;
} 	
	
	
public String toString() {
	
	StringBuilder sb = new StringBuilder();
	
	sb.append( E+"Edjes"+ V + "Vertex" + "\n") ;
	
	for(int v = 0 ; v < V ; v++) {
		
		sb.append(v + " : ") ;
		
		for(int w : adj[v]) {
			
			sb.append(w + " ") ;
		}
		
		sb.append("\n") ;
	}

	return sb.toString();
	
}


public void bfs(int s) {
	
boolean[] visited = new boolean[V] ;	
	
    Queue<Integer>  qq = new LinkedList<Integer>();
	
     visited[s] = true ;	
	
	qq.offer(s) ;
	
	while(!qq.isEmpty()) {
		
		int u = qq.poll();
		
		System.out.print(u + " ");
		
		for(int v : adj[u]) {
			
			if(!visited[v]) {
				
				visited[v] = true ;
				
			     qq.offer(v) ;
			
			}
			
		}
	}
	
	
}


public void dfs(int s) {
	
	boolean[] visited = new boolean[V] ;
	
	Stack<Integer> ss = new Stack<Integer>();
	
	  ss.push(s) ;
	
	  while(!ss.isEmpty()) {
		  
		  int u = ss.pop() ;
		  
		  if(!visited[u]) {
			  
			  visited[u] = true ;
			  
			  System.out.print(u + " ");
			  
			  for(int v : adj[u]) {
				  
				  if(!visited[v]) {
					  
					  ss.push(v) ;
				  }
			  }
		  }
		  
		  
	  }
}


public static void main(String[] args) {
	
DEMO_BFS1 g = new DEMO_BFS1(4) ;

g.addEdge(0, 1);

g.addEdge(1, 2);	

g.addEdge(2, 3);

g.addEdge(0, 3);


//System.out.println(g);

//g.bfs(0);

g.dfs(0);

}


}

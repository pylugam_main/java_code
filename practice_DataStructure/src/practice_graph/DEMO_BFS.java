package practice_graph;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class DEMO_BFS{
	
private LinkedList<Integer> adj[] ;	
private int V;	
private int E ;	

public DEMO_BFS() {
	// TODO Auto-generated constructor stub
    this(10);
}

public DEMO_BFS(int nodes) {
	// TODO Auto-generated constructor stub
this.V = nodes ;
this.E = 0 ;
this.adj = new LinkedList[nodes] ;

for(int v = 0 ; v < V ; v++) {
	adj[v] = new LinkedList<Integer>();
}

}	
	
public void addEdge(int u , int v) {
	
	adj[u].add(v) ;
	adj[v].add(u) ;

	E++ ;
} 	
	
public String toString() {
	
	StringBuilder sb = new StringBuilder();
	System.out.println(V + "Vertax" + E + " Edjes");
	
	for(int v = 0 ; v < V ; v++) {
		sb.append(v + ":") ;
		
		for(int w : adj[v]) {
			sb.append(w + " ");
		}
		
		sb.append("\n");
	}
	return sb.toString();
	
}	
	
// level by order
public void bfs(int s) {
	
	boolean[] visited = new boolean[V];
	Queue<Integer> qq = new LinkedList<Integer>();
	visited[s] = true ;
	qq.offer(s);
	while(!qq.isEmpty()) {
	int u = qq.poll();
	System.out.println(u + " ");
	for(int v : adj[u]) {
		if(!visited[v]) {
			visited[v] = true ;
			qq.offer(v);
		}
	}
	
}
}	
	
public void dfs(int s) {

	boolean[] visited = new boolean[V];	
    Stack<Integer> stack = new Stack<Integer>();
    stack.push(s);
    while(!stack.isEmpty()) {
    int u = stack.pop();	    
    if(!visited[u]) {
    visited[u] = true ;
    System.out.println(u);
     }
    for(int w : adj[u]) {
    	
    	if(!visited[w]) {
    		stack.push(w);    	
    	}
    }
    }
	
	
}	
	
	
	
	
	

















public static void main(String[] args) {
	
DEMO_BFS D = new DEMO_BFS(4);	

D.addEdge(0, 1);	
D.addEdge(1, 2);
D.addEdge(2, 3);	
D.addEdge(3, 0);

//System.out.println(D);

//D.bfs(0);

//D.dfs(0);































}	
	
	
	
	
	
}
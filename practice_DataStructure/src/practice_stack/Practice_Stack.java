package practice_stack;

import java.util.EmptyStackException;

public class Practice_Stack {

	private ListNode top ;
	
	private int length ;
	
	
private class ListNode{
	
	private int data ;
	
   private ListNode next ;	
	
	
	public ListNode(int data) {
		
		this.data = data ;
		
	}
	
}	
	
public Practice_Stack() {
	// TODO Auto-generated constructor stub

        top = null ;

        length = 0 ;
}

public int length() {
	
	return length ;
}


public boolean isEmpty() {
	
	return length == 0 ;
}


public void push(int value) {
	
   ListNode node = new ListNode(value) ;	
	
   node.next = top;  
   
   top = node ;
   
   length++ ;
   
}


public int pop() {
	
	if(isEmpty()) {
		
		throw new IllegalArgumentException("empty");
	}
	
	int result = top.data ;
	
	top = top.next ;
	
	length--;
	
	return result ;
}


public int peek() {
	
	
	if(isEmpty()) {
		 
		 throw new EmptyStackException() ;
	 }
	 

return top.data ;
}
public static void main(String[] args) {
		// TODO Auto-generated method stub

	Practice_Stack stack = new Practice_Stack() ;
	
	stack.push(2);

	stack.push(4);
	
	stack.push(8);
	 
	System.out.println(stack.pop());
	
	}

}

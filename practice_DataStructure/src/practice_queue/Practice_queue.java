package practice_queue;

public class Practice_queue {

	private ListNode front ;
	
	private ListNode rear ;
	
	private int length ;
	
	
	public Practice_queue() {
		
		this.front = null ;
		
		this.rear = null ;
		
		this.length = 0 ;
	}
	
	
private class ListNode{
	
	private int data ;
	
	private ListNode next ;
	
	
	public ListNode(int data) {
		
		this.data = data ;
	}
	
	
}	


public int length() {
	
	
	return length ;
}	
	

public boolean isEmpty() {
	
	return length == 0 ;
}

public void enque(int value) {
	
	ListNode temp = new ListNode(value) ;
	
	if(isEmpty()) {
		
		front = temp ;
	}
	else {
		
		rear.next = temp ;
	}
	
	  rear = temp ;
	  
	  length++ ;
}

	
public void  display() {
	
 if(isEmpty()) {
		 
		 return ;
	 }

	ListNode current = front ;
	
	while(current != null) {
		
		System.out.print(current.data );
		
		current = current.next ;
	}
	
	System.out.print("null");
}

public int deque() {
	
   if(isEmpty()) {
	   
	   throw new IllegalArgumentException("empty");
	   
   }
	
	int result = front.data ;
	
	front = front.next ;
	
	if(front == null) {
		
		rear = null ; 
	}
	
	length-- ;


  return result ;
}


public int first() {
	
	if(isEmpty()) {
		
		throw new IllegalArgumentException("empty");
	}
	
	return front.data ;
}

public int last() {
	
	if(isEmpty()) {
		
		throw new IllegalArgumentException("empty");

	}

    return rear.data ;
    
}


public static void main(String[] args) {
		// TODO Auto-generated method stub

Practice_queue q = new Practice_queue()	;	
		
   q.enque(44);

   q.enque(23);
   
   q.display();
    
   
	}

}

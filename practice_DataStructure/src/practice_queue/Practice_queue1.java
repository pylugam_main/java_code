package practice_queue;

public class Practice_queue1 {

	private ListNode front ;
	
	private ListNode rear ;
	
	private int length ;
	
private class ListNode{
	
	private int data ;
	
	private ListNode next ;
	
	public ListNode(int data) {
		// TODO Auto-generated constructor stub
	this.data = data ;
	
	}
	
}	
	
 public int length() {
	 
	 return length ;
	 
 }	
	
	
 public boolean size() {
	 
	 return length == 0 ;
	 
 }
 
 
 public void enqueue(int data) {

	 ListNode node = new ListNode(data) ;
	 
	 if(front == null) {
		 
		 front = node ;
	 }
	 else {
		 
		 rear.next = node ;
	 }
	 
	 rear = node ;
	 
	 length++ ;
	 
 }
 
 public int dequeue() {
	 
	 int result = front.data ;
	 
	 front = front.next ;
	 
	return result ;
	
 } 
 
 public static void main(String[] args) {
	
	Practice_queue1 ll = new Practice_queue1(); 
	 
	ll.enqueue(23);
	
	ll.enqueue(78);
	
	ll.enqueue(90);
	
	System.out.println(ll.dequeue());

	System.out.println(ll.dequeue());

 
 
 }
 
}
package practice_linkedlist;

import java.util.NoSuchElementException;

public class Practice_Doubly_LInked_List {

	private ListNode head ;
	
	private ListNode tail ;
	
	private int length ;
	
	
	public Practice_Doubly_LInked_List() {
		// TODO Auto-generated constructor stub
	this.head = null ;
	
	this.tail = null ;
	
	this.length = 0 ;
	
	}
	
private class ListNode{

	private int data ;
	
	private ListNode next ;
	
	private ListNode previous ;
	
	public ListNode(int value) {
		// TODO Auto-generated constructor stu
	this.data = value ;
	
	this.next = null ;
	
	this.previous = null ;
	
	}
	
	
}

public boolean  isEmpty() {
	
	return length == 0 ;
}


public void insertfirst(int value) {
	
	ListNode node = new ListNode(value) ;
	
	if(isEmpty()) {
		
		tail = node ;
	}
	else {
		
		head.previous = node ;
		
	}
	node.next = head ;
	
   node = head ;
   
   length++ ;
   
}


public void insertlast(int value) {
	
	ListNode node = new ListNode(value) ;
	
	if(isEmpty()) {
		
	  head = node ;
	}
	else {
		
		tail.next = node ;
		
		node.previous = tail ;
	}
	
	tail = node ;
	
	length++ ;
	
}

public ListNode deletefirst() {
	
	
	if(isEmpty()){
		
		throw new NoSuchElementException();
	}
	
	if(head == null) {
		
		tail = null ;
	}
	else {
		
		head.next.previous = null ;
		
	}
	
	ListNode temp = head ;
	
	head = head.next ;

	temp.next = null ;
	
	length-- ;
	
	return temp ;
}


public ListNode deletelast() {
	
if(isEmpty()){
		
		throw new NoSuchElementException();
	}
	
	if(head == null) {
		
		tail = null ;
	}
	else {
	
      tail.previous.next = null ;	
	
}

    tail = tail.previous ;

   ListNode temp = tail ;

     tail.previous = null ;

length-- ;

       return temp ;
}

}
package practice_linkedlist;

import java.util.LinkedList;

public class PracticeSingleLinkedlist {

  private static ListNode head ;	
		
private static class ListNode{
	
	private int data ;
	
	private ListNode next ;

	public int val;
	
	
	
 public ListNode(int value) {
	 
	 this.data = value ;
	 
	 this.next = null ;
	 
 }	
	
}	


public void display(ListNode head) {
	
	ListNode current = head ;
	
	while( current != null) {
		
		System.out.print(current.data +"---->");
	
	   current = current.next ;
	}
	
   System.out.println("null");	
}	

public void insertfirst(int value) {
	
	ListNode newnode = new ListNode(value) ;
	
	newnode.next = head ;
	
	head = newnode ;
}


public int length(ListNode head) {
	
	ListNode current = head ; 
	
	int count = 0 ;
	
	while(current != null) {
	 	
  	 count++ ;
		
		current = current.next ;
	}

	return count ;
}


public void insertlast(int value) {
	
	
	ListNode newnode = new ListNode(value) ;
	
	if(head == null) {
		
		head = newnode ;
		
		return ;
	}
	
	ListNode current = head ;
	
	while(null != current.next) {
		
		current = current.next ;
		
	}
	
	current.next = newnode ;
	
}

public void insertposition(int position,int value) {
	
	ListNode node = new ListNode(value) ;
	
	if(position == 1) {
		
		node.next = head ;
		
		head = node ;
	}
	else {
		
		ListNode previous = head ;
		
	int	count = 1 ;
		
		while(count < position -1) {
			
			previous = previous.next ;
			
			count++ ;
		}
		
	ListNode current = previous.next ;
	
	previous.next = node ;
	
	 node.next = current ;
	}
	
}


public ListNode removefirst() {
	
	if(head == null) {
		
		return null ; 
	}
	
	
   ListNode temp = head ;	
	
   head = head.next ;
   
	temp.next = null  ;
			
	
	return temp ;
}

public ListNode delelelast(){
	
	if(head == null || head.next == null) {
		
		return head ;
	}
	
	ListNode current = head ;
	
	ListNode previous = null ;
	
	while( current.next != null) {

	previous = current ;	
	
	current = current.next ;
	
	}
	
	previous.next = null ;
 	
	return current ;

	
}

public void  delete(int position) {
	

	if(1 == position) {
		
		head = head.next ;
	}
	else {
   ListNode previous = head ;

   int count = 1 ;

  while(count < position -1) {
	  
	  
	  previous = previous.next ;
	  
	  count++ ;
  }
  
	
   
  ListNode current = previous.next ; 
  
  previous.next = current.next ;
  
}
	
}


public ListNode mergeKLists(ListNode[] lists) {
    if (lists == null || lists.length == 0) {
        return null;
    }
    return mergeKListsHelper(lists, 0, lists.length - 1);
}

private ListNode mergeKListsHelper(ListNode[] lists, int start, int end) {
    if (start == end) {
        return lists[start];
    }
    if (start + 1 == end) {
        return merge(lists[start], lists[end]);
    }
    int mid = start + (end - start) / 2;
    ListNode left = mergeKListsHelper(lists, start, mid);
    ListNode right = mergeKListsHelper(lists, mid + 1, end);
    return merge(left, right);
}

private ListNode merge(ListNode l1, ListNode l2) {
    ListNode dummy = new ListNode(0);
    ListNode curr = dummy;
    
    while (l1 != null && l2 != null) {
        if (l1.val < l2.val) {
            curr.next = l1;
            l1 = l1.next;
        } else {
            curr.next = l2;
            l2 = l2.next;
        }
        curr = curr.next;
    }
    
    curr.next = (l1 != null) ? l1 : l2;
    
    return dummy.next;
}

public static void main(String[] args) {
		
PracticeSingleLinkedlist ll = new PracticeSingleLinkedlist();
	  
//     ll.head = new ListNode(3) ;   		
//	
//     ListNode second = new ListNode(4) ;
//     
//     ListNode third = new ListNode(3) ;
//     
//     ListNode fourth = new ListNode(4) ;
//		
//
//     
//     ll.head.next = second ;
//     
//     second.next = third ;
//     
//     third.next = fourth ;
//        
//     
     
    // ll.insertfirst(11);
     
    //  ll.insertlast(234);
     
   //  ll.display(head);
     
   //  ll.insertposition(6, 0);
     
   //  ll.display(head);

   //  System.out.println(ll.removefirst().data);
     
   //  ll.display(head);
     
    // ll.delelelast();
     
 //   ll.delete(1);
     
  //   ll.display(head);
  //   
 //    System.out.println(ll.length(head));
     
    // ListNode lists = {1,2,4,5} ;
     
   LinkedList<Integer> lll = new LinkedList<Integer>() ;
   
          lll.add(1);
          lll.add(4);
          lll.add(5);
          
          lll.add(1);
          lll.add(3);
          lll.add(4);
          
          lll.add(2);
          lll.add(6);
          
         ListNode[] arr = lll.toArray(new ListNode[0]);
          
           ll.mergeKLists(arr) ; 
     
  ///  ll.display(head);
     
     
	}
	
	
	
	
}

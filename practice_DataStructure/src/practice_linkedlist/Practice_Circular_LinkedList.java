package practice_linkedlist;

import java.util.NoSuchElementException;

public class Practice_Circular_LinkedList {

private ListNode last ; 
	
private int length ;	


private class ListNode{
	
	private int data ;
	
	private ListNode next ;
	
	
	public ListNode(int value) {
		// TODO Auto-generated constructor stub
	
		this.data = value ;
	
	}
	
	
	
}

	
public Practice_Circular_LinkedList() {
	// TODO Auto-generated constructor stub
this.last = null ;

this.length = 0 ;

}	
	
	
public void createlinkedlist() {
	
	ListNode first = new ListNode(1) ;
	
	ListNode second = new ListNode(2) ;

	ListNode third = new ListNode(3) ;

	ListNode fourth = new ListNode(4) ;

	first.next = second ; 
	
	second.next = third ; 
	
	third.next = fourth ;
	
	fourth.next =first ;
	
	last = fourth ;
	
}	
	
public void display() {
	
	ListNode first = last.next ;
	
	while(first != last) {
		
		System.out.print(first.data);
		
		first = first.next ;
	}
	
	System.out.println(first.data);

}	
	
	
public void insrfirst(int value) {
	
	ListNode node = new ListNode(value) ;
	
	if(last == null) {
		
		last = node ;
	}
	else {
		
		node.next  = last.next ;
	}
	
	last.next = node ;
	
	length++ ;
	
}


public void insertlast(int value) {
	
	ListNode node = new ListNode(value) ;
	
	if(last == null) {
		
		last = node ;
		
		last.next = last ;
		
	}
	else {
		
		node.next = last.next ;
	}
	
	last.next = node ;
	
	last = node ;
	
	length++ ;
}
	
public boolean isEmpty() {
	
	return length == 0 ;
	
}


public ListNode removefirst() {
	
	if(isEmpty()) {
		
		throw new NoSuchElementException();
	}
	
	ListNode temp = last.next ;
	
	if(last.next == last) {
		
		last = null ;
	}
	else {
		
		last.next = temp.next ;
	}
	
	temp.next = null ;
	
	length-- ;
	
	return temp ;
}
	
}
package Algorithms;

public class Practice_searchalgo {

public int binarysearch(int[] num,int target) {
	
 int low = 0 ;
int high = num.length - 1 ;

	while(low<high) {
		int mid = (low + high) / 2 ;
		
		if(num[mid] == target) {
			return mid ;
		}
		
		if(num[mid] > target) {
			high = mid - 1 ;
			
		}else {
			low = mid + 1 ;
		}
	}
	
	return -1 ;
	
}	


public int leanearsearch(int[] num ,int target) {

	int n = num.length ;
	
	for(int i = 0 ; i < n ; i++) {
		
		if(num[i] == target) {
			return i ;
		}
	}
	
	return -1 ;
	
	
}


public static void main(String[] args) {
	
	Practice_searchalgo P = new Practice_searchalgo();
	
	int[] num = {1,2,3,4,5,6,7,8,9,10};
	
	System.out.println(P.binarysearch(num, 9));

	System.out.println(P.leanearsearch(num, 9));

}

}

package Algorithms;

public class Demo_Sort {

public void bubblesort(int[] numbers) {
	
	boolean istrue ;
	
	for(int i = 0 ; i < numbers.length ; i++) {
		istrue = false ;
		
		for(int j = 0  ; j < numbers.length - 1 - i ; j++) {
		
		if(numbers[j] > numbers[j + 1]) {
			
			int temp = numbers[j] ;
           numbers[j] = numbers[j + 1] ;
           numbers[j+1] = temp ;
           istrue = true ;
		
		}
		
	}	
		
		if(istrue == false) {
			break ;
		}
	}
		
}	


public void sort(int[] arr) {
	
	for(int i = 0 ; i < arr.length ; i++) {
		
		System.out.print(arr[i]);
	}
	
} 
	
public void insertionsort(int[] numbers) {
	
	for(int i = 1 ; i < numbers.length ; i++) {
		
		int temp = numbers[i] ;
		
		int j = i - 1 ;
		
	while(j >= 0 && numbers[j] > temp) {
		
		numbers[j+ 1] = numbers[j] ;
         j = j - 1 ;
		
	}
	
	numbers[j + 1 ] = temp ;
	
	}
	
	
}	


public void selectionsort(int[] num) {
	
   for(int i =0 ; i < num.length ; i++) {
	   
	   int min = i ;
	   
	for(int j =i + 1; j < num.length ; j++) {
		
		if(num[min] > num[j]) {
			
			min = j ;
		}
	}
	
	int temp = num[min] ;
	
	num[min] = num[i] ;
	
	num[i] = temp ;
	
   }	

}

	public static void main(String[] args) {
		
	int[] num = {6,3,9,1,0,2,7,3,4,7,6,8,4,2};	
		
	Demo_Sort DS = new Demo_Sort();
//	DS.bubblesort(num) ;
//    DS.sort(num);

//    DS.insertionsort(num);
//    DS.sort(num);
    
	
	DS.selectionsort(num);
	DS.sort(num);
	
	}
	
	
	
	
}

package Algorithms;

public class Demo_Search {

	//
public int binarysearch(int[] arr , int target) {
	
	int low = 0 ;
	int high = arr.length - 1 ;
	
	while(low <= high) {
             int mid = (low + high) / 2 ;  		
		
		if(arr[mid] == target) 
		return mid ;
		
		
		if(arr[mid] > target)
			high = mid - 1 ;
		else
			low = mid + 1 ;
	}
	
	return - 1 ; 
}
	

public int leanersearch(int[] arr , int target) {
	
	for(int n = 0 ; n < arr.length ; n++) {
		
		if(target == arr[n]) {
			return n ;
		}
		
	}
	
	return -1 ;
}
	


public static void main(String[] args) {
		// TODO Auto-generated method stub

		Demo_Search ds = new Demo_Search();
		
		int[] a = {1,2,3,4,5,6,7,8,9};
     	int target = 9 ;
//	System.out.println(ds.binarysearch(a,target));	
	
	    System.out.println(ds.leanersearch(a, target));
	
	}

}

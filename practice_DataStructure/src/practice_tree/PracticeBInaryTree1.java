package practice_tree;

public class PracticeBInaryTree1 {

 private TreeNode root ;	
	
	
private class TreeNode{
	
	private TreeNode left ;
	
	private TreeNode right ;	
		
    private int data ;
	
	
    public TreeNode(int data) {
		// TODO Auto-generated constructor stub
	this.data = data ;
    
    }
	
}	
	
	
public void createBinaryTree() {
	
	TreeNode first = new TreeNode(21) ;
	
	TreeNode second = new TreeNode(26) ;
	
	TreeNode third = new TreeNode(45) ;
	
	TreeNode fourth = new TreeNode(78) ;
	
	TreeNode eight = new TreeNode(798) ;
	
	
	root = first ;
	
	first.left = second ;
	
    first.right = third ;	
	
	second.left = fourth ;
	
	second.right = eight ;
}	
	
public void inOrder(TreeNode root) {
	
  if(root == null) {
	  
	  return ;
  }
	
  inOrder(root.left) ;

System.out.print(root.data + " ");

inOrder(root.right);


}	
	
	
public void postOrder(TreeNode root) {
	
	if(root ==  null) {
		
		return ;
	}
	
	
	postOrder(root.left);

	postOrder(root.right);
	
	
	System.out.print(root.data + "  ");
	
}

public void preOrder(TreeNode root) {
	
	if(root == null) {
		
		return ;
	}
	
	System.out.print(root.data +" ");

	preOrder(root.left);
	
	preOrder(root.right);
	

}


public void a() {
	
	int a = 1 ;
	
	if(root == null) {
		
		return  ;
	}
}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	PracticeBInaryTree1 bt = new PracticeBInaryTree1() ;
		
	  bt.createBinaryTree();
	
//	   bt.inOrder(bt.root);
	  
	//  bt.postOrder(bt.root);
	
	  
	  bt.preOrder(bt.root);
	}

}

package practice_tree;

public class PracticeBinaryTree {

   private TreeNode root ;
	
	
private class TreeNode{
	
	private TreeNode left ;
	
	private TreeNode right ;
	
	private int data ;
	
	
	public TreeNode(int data) {
		
		this.data = data ;
		
	}
	
	
}

public void createbinarytree() {
	
	TreeNode first = new TreeNode(1) ;
	
	TreeNode second = new TreeNode(2) ;
	
	TreeNode third = new TreeNode(3) ;
	
	TreeNode fourth = new TreeNode(4) ;
	
	root = first ;
	
	first.left = second ;
	
	first.right = third ;
	
	second.left = fourth ;
	
	
}

public void preOrder(TreeNode root) {
	
	if(root == null) {
		
		return ;
	}
	
	System.out.print(root.data + " ");
	
	preOrder(root.left);
	
	preOrder(root.right);
}


public void inOrder(TreeNode root) {
	
	
	if(root == null) {
		
		return ;
	}
	
	inOrder(root.left);
	
	System.out.print(root.data + " ");
	
	inOrder(root.right);
	
}


public void postorder(TreeNode root) {
	
	if(root == null) {
		
		return ;
	}
	postorder(root.left);
	
	
	postorder(root.right);
	
	System.out.print(root.data + " ");
	
} 

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		PracticeBinaryTree bt = new PracticeBinaryTree() ;
		
		bt.createbinarytree();
		
	//   bt.preOrder(bt.root);
		
	//	bt.inOrder(bt.root);
	
	
		bt.postorder(bt.root);
	}

	
	
	
}

package practice_hash;

public class Priortyqueue {

	private Integer[] heap ;
	
	private int n ;
	
	
	public Priortyqueue(int capacity) {
		// TODO Auto-generated constructor stub
	heap = new Integer[capacity + 1] ;
	
	n = 0 ;
	
	}
	
	
	public boolean isEmpty() {
		
		return n == 0 ;
		
	}
	
	
	public int size() {
		
		return n ;
	}
	
	public void insert(int x) {
		
		if(n == heap.length - 1) {
			
			resize(2 * heap.length ) ;
		}
		
		n++ ;
		
		heap[n] = x ; 
		
		swim(n) ;
	}

	
	public void swim(int k) {
		
		while( k > 1 && heap[k / 2] < heap[k]) {
			
			int temp  = heap[k] ;
			
			heap[k] = heap[k/2] ;
			
			heap[k / 2] = temp ;
			
			k = k / 2 ;
		}
		
		
	}
	

	public void resize(int capacity) {
		
	Integer [] temp = new Integer[capacity] ;
	
	for(int i = 0 ; i < heap.length ; i++) {
		
		temp[i] = heap[i] ;
	}
	
		heap = temp ;
		
	}
	
	
	public void  print() {
		
		for(int i = 1 ; i <= n; i++) {
			
			System.out.print(heap[i] + " ");
			
		}
		
	}
	
	
	
	public static void main(String[] args) {
		
	Priortyqueue k = new Priortyqueue(0) ;
	
	k.insert(3);
		
	k.insert(4);
	
	k.insert(7);
	
	k.print();
	
	}
	
}

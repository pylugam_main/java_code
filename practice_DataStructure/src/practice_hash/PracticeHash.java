package practice_hash;

public class PracticeHash {

	private HashNode[] buckts ; 
	private int numofbucks ;
	private int size ;
	

	public PracticeHash() {
		// TODO Auto-generated constructor stub
	     this(10);
	}
	
	PracticeHash(int capacity){
		this.numofbucks = capacity ;
		this.buckts = new HashNode[numofbucks];	
	    this.size = 0 ; 
	
	}
	
private class HashNode{
	
	private Integer key ;
	private String Value ;
	private HashNode next ;
	
	
	public HashNode(Integer key,String value) {
		// TODO Auto-generated constructor stub
	this.key = key ;
	this.Value = value ;
	this.next = null ;
	
	}
	
}	

public void put(Integer key,String Value) {
	
	if(key == null) {
	throw new IllegalArgumentException("key is null");	
	}
	
	int bucketindex = getbucketindex(key);
	HashNode head = buckts[bucketindex];
	
	while(head != null) {
		
		if(head.key.equals(key)) {
			head.Value = Value;
		}
		
		head = head.next ;
	}
	
 head = buckts[bucketindex];	
HashNode node = new HashNode(key, Value);	
	node.next = head;
	buckts[bucketindex] = node;
		
}


public String remove(Integer key) {
	
	if(key == null) {
		throw new IllegalArgumentException("key is null");	
		}
		
	int bucketindex = getbucketindex(key);
	HashNode head = buckts[bucketindex];
	HashNode previous = null ;
	
	while(head != null) {
		
	if(head.key.equals(key)) {
		break ;	
	}
       previous = head ;	
	head= head.next ;

	}
	
	head = buckts[bucketindex];
	
	if(previous != null) {
		previous.next = head.next ;
	}
	else {
		buckts[bucketindex] = head.next ;		
	}
	
	return head.Value ;
}


public int getbucketindex(int key) {
	
	return key % numofbucks ;
}

public String get(Integer key) {
	
	if(key == null) {
		throw new IllegalArgumentException("key is null");	
		}
		
	int bucketindex = getbucketindex(key);
	HashNode head = buckts[bucketindex];
	
	while(head != null) {
		
		if(head.key.equals(key)) {
			
			return head.Value ;
		}
		
		head= head.next ;
	}
	
	return null ;
}


public static void main(String[] args) {
	
PracticeHash l = new PracticeHash() ;	
	
l.put(4, "hi");

l.remove(4);

System.out.println(l.get(4));



}

}
package java8.functional_interface;

public class members {

	
	public static void main(String[] args) {
		
		
		// this method only lamda expression
		perform p = () ->{System.out.println("hi");};
		p.test();
		
		
		// this method only argument n1,n2
	  show s = (n1,n2) -> { System.out.println(n1+n2); };
         s.show(1, 2);
	
	  
       // this method return and argument n1 + n2  
       members1 s1 = (n1,n2) ->{ return n1+n2; };
	  
	    s1.add(2, 2);
	
	
	
	
	
	
	
	
	
	
	}
	
}

package practice_stream_api;

import java.util.ArrayList;
import java.util.List;

public class CollectionUseAPI {

public static void main(String[] args) {
	
List<Integer> l = new ArrayList<Integer>();	
	l.add(2);
	l.add(2);
	l.add(2);
	l.add(2);
	l.add(5);
	
	//print count value
	System.out.println(l.stream().count());
	
	// print unique values
	l.stream().distinct().forEach(System.out::println);
	
	
	
List<String> ll = new ArrayList<String>();

	ll.add(" A abcd");
	
	ll.add(" B fghl");
	
	ll.add(" C dsks");
	
	ll.add(" D hknh");

	ll.add(" E zmxcl");
	
//print reverse order 	
	ll
	.stream()
	.distinct()
	.sorted((n1,n2) -> -5)
	.forEach(System.out::println);
	
	
//	manual sorting compareto
	ll
	.stream()
	.distinct()
	.sorted((n1,n2) ->n2.compareTo(n1))
	.forEach(System.out::println);
	
	
	
	
	
	
}


}

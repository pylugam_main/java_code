package practice_stream_api;

import java.util.Arrays;
import java.util.OptionalDouble;
import java.util.OptionalInt;

public class IntermediateFunction {

public static void main(String[] args) {
	
	int[] ar = {4,3,8,6,1,5,8,3};
	
	//this statemennt print optimal double
	System.out.println(Arrays.stream(ar).sorted().average());
	
	//this statement print averge value
	OptionalDouble ad = Arrays.stream(ar).average();
	System.out.println(ad.getAsDouble());
	
	// this statement print max value in array
	OptionalInt oi = Arrays.stream(ar).max();
	System.out.println(oi.getAsInt());
	
	//this satement print  first value
	OptionalInt F = Arrays.stream(ar).findFirst();
	System.out.println(F.getAsInt());
	
	// this statement print min value in array
	OptionalInt A = Arrays.stream(ar).min();
	System.out.println(A.getAsInt());

	
	
	int[] al = {20,6,90,4,3,1,6,8,4};
	
	//print the unique element in array
	Arrays.stream(al).distinct().forEach(System.out::print);
	
	// print even numbers
	Arrays.stream(al).filter(n -> n % 2 == 0)
	.forEach(System.out::println);

}

}

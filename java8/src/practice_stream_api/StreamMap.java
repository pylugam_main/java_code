package practice_stream_api;

import java.util.ArrayList;
import java.util.List;

public class StreamMap {

public static void main(String[] args) {
	
	List<String> ll = new ArrayList<String>();

	ll.add(" A abcd");
	
	ll.add(" B fghl");
	
	ll.add(" C dsks");
	
	ll.add(" D hknh");

	ll.add(" E zmxcl");
	
// print value uppercase	
	ll
	.stream()
	.map(word -> word.toUpperCase())
	.forEach(System.out::println);
	
	
	
	ArrayList<Integer> al = new ArrayList<Integer>();	
	al.add(2);
	al.add(2);
	al.add(2);
	al.add(2);
	al.add(5);	
	
// print divide by 2	
	al
	.stream()
	.map(n2 -> n2 / 2 )
	.forEach(System.out::println);
	
	
	
	ArrayList<String> l1 = new ArrayList<String>();	
	l1.add("kavin") ;
	l1.add("vijay");
	
// add the last value	
	l1
	.stream()
	.map(n -> n + "B.E...")
	.forEach(System.out::println);
	
	
}	
	
}

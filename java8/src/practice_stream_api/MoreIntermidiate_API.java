package practice_stream_api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class MoreIntermidiate_API {

public static void main(String[] args) {
	
HashMap<String, Integer> hm = new HashMap<String, Integer>();	
	
	hm.put("vijay", 102);
	
	hm.put("kavin", 100);
	
	hm.put("kannan", 108);
	
	hm.put("arun", 190);
	
	
// sort string look first letter	
   hm	
	.keySet()
	.stream()
	.sorted()
	.forEach(System.out::println);
	
	
// sorting by value	
   hm	
  	.entrySet()
  	.stream()
  	.sorted(Map.Entry.comparingByValue())
  	.forEach(System.out::println);
  	

   
//	print sorted string	
   hm	
 	.entrySet()
 	.stream()
 	.sorted(Map.Entry.comparingByKey())
 	.forEach(System.out::println);
	

//convert arraylist   
List<String> l = hm.keySet().stream()
                .collect(Collectors.toList());	
   
   
   System.out.println(l);
   
   
   
   
  // print boolean in match 
 boolean k =  l
   .stream()
   .anyMatch(name -> name.endsWith("n"));
   System.out.println(k);
   
	
}
	
}

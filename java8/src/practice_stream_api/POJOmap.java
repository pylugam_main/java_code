package practice_stream_api;

import java.util.ArrayList;
import java.util.Optional;
import java.util.stream.Stream;

public class POJOmap {

public static void main(String[] args) {
	
 ArrayList<POJO_Student> l = new ArrayList<POJO_Student>();	
	
POJO_Student e1 = new POJO_Student("kavin",240);
	
POJO_Student e2 = new POJO_Student("khhhhhh",2400);
	
POJO_Student e3 = new POJO_Student("keeeeee",2400);
	
	
	l.add(e1);
	l.add(e2);
	l.add(e3);
	
// a already consumed so illegal state exception so 	
// we need comment 28,30 line	
// print string names	
	Stream<POJO_Student> a = l.stream();
	
 //  Stream<String> empnamestream = a.map(emp -> emp.getName());
   
//   empnamestream.forEach(System.out::println);
   
   
   // print name based condition
  Stream<POJO_Student> expempstream = a.filter(emp1 -> emp1.getMark() < 500);
   
  Stream<String> empName2Stream =  expempstream.map(emp1 -> emp1.getName());
		   
  empName2Stream.forEach(System.out::println);		   
		   
  
  
  
  ArrayList<Integer> al = new ArrayList<Integer>();	
	al.add(1);
	al.add(2);
	al.add(3);
	al.add(4);
	al.add(5);	

// print first 2 numbers	
//  al
//  .stream()
//  .limit(2)
//  .forEach(System.out::println);
  
// print total value  
// Optional<Integer> result = al.stream().reduce((n1,n2) -> n1+n2);
// System.out.println(result.get());  
//  
  
Object[] ob = al.stream().toArray();
	
//	for(Object o : ob) {
//		System.out.println(o);
//	}
	
	
	// print greater value
Optional<Integer> result = al.stream().max((e4,e5) -> e4.compareTo(e5));	
	System.out.println(result.get());
	
// print value parrelstream 	
	al.parallelStream()
	.forEachOrdered(System.out::println);
	
//parallel() mathod convert parallel stream
	al.stream().parallel()
	.forEachOrdered(System.out::println);
	
	
// is isParallel() check parallel or not
	al.stream().isParallel();

}


}

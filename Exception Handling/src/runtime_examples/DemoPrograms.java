package runtime_examples;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class DemoPrograms {

public void Arithmetic(int Value ) {
	
	int diviser = 0 ,op ;
	
   op = Value / diviser ;	
  System.out.println(op);

}	
	
	
public void IndexBound() {
	
	int[] box = new int[5];
	
	for(int i = 0 ; i < 6 ; i++) {
		
		System.out.println(box[i]);
	}
}	
	
	
public void NullPoienter() {
	
	String s = null ;
	System.out.println(s.length());
}	
	
// what is parseInt
public void NumberFormat() {
	
	String num= "abc" ;
	
	int c = Integer.parseInt(num);
	
	System.out.println(c);
	
	
}	
	
// invalid Type Casting	
public void classcast () {
	
	Object ob = new Integer(100) ;
	
	String s = (String) ob ;
	
	System.out.println(s);
}
	
//illegalArgument passed	
public void illegaleArgument() {
	
	Thread th = new Thread();
	th.setPriority(200);
	
	
}	


public void illegalstate() {
	
ArrayList<String> list = new ArrayList<String>();	
	
	list.add("hellow");
	list.add("welcome") ;
	list.add("good morning") ;
	
	Iterator<String> i = list.iterator();
// illegal state because remove called before next	
	i.remove();
}	
	


public void negativeArray() {
	
	int[] arr = new int[-34];
	
}	
	
	
public void StringIndexoutOfBounds() {
	
	String str = "Hellow" ;
	
	char ch = str.charAt(56);
	
	System.out.println(ch);
	
	
}

public void unsupportedOperation() {
	
List<String> ls = Arrays.asList("A","B","C","D") ;	
	
ls.add("hj");	
	
}



public void nosuchElement() {
	
ArrayList al = new ArrayList();	
	
	Iterator<String> ir = al.iterator();
	
	String element = ir.next();
	System.out.println(element);

}


public void moniterState(){
	
	Object lock = new Object();
	lock.notify();


}

public void ConcurrentModification(){
	
	ArrayList<String> list1 = new ArrayList<String>();
	
	list1.add("A");
	list1.add("B");
	
for(String element : list1) {
	
	if(element.equals(element)) {
		
		list1.remove(element);
	}
		
}	
		
}


//stackOverFlowError
public void  stackoverflow() {
	
	stackoverflow();

}


//outOfMemory
public void OutofMemory(){
	
ArrayList<int[]> list = new ArrayList<int[]>();	
	
	while(true) {
		
		list.add(new int[100000]);
	}
}


public void NoclassDefFound(){
	
	MyNonExistance obj = new MyNonExistance();
	
}


public void classnotfound() {
	
	try {
		
		Class.forName("HI");
	}
	catch (Exception e) {
		// TODO: handle exception
	   e.printStackTrace();
	}
	
}

public void illegalThreadStates() {
	
	
	Thread th = new Thread();
	
	th.start();
	th.start();
	
}

public void arrayStore() {
	
	
	Object[] arr = new String[9];
	
	arr[0] = 100 ;
	
}



























public static void main(String[] args) {
	
	DemoPrograms D = new DemoPrograms();
	
//	D.Arithmetic(23);
 //   D.IndexBound();	
 //   D.NullPoienter();
 // D.NumberFormat();




}	
	
	
	
	
}

package com.learning;

import static org.junit.Assert.*;

import org.junit.Test;

public class CalcTest {

	@Test
	public void test() {
		Calc ll = new Calc() ;
		
		int actual = ll.divide(10, 5) ;
		
		int expected = 2 ;
		assertEquals(expected, actual);
	}

}

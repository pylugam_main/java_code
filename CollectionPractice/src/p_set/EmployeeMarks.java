package p_set;

public class EmployeeMarks implements Comparable<EmployeeMarks> {

	private int Maths ;
	private int Physics ;
	
	public int getMaths() {
		return Maths;
	}
	public void setMaths(int maths) {
		Maths = maths;
	}
	public int getPhysics() {
		return Physics;
	}
	public void setPhysics(int physics) {
		Physics = physics;
	}
	
	
	public EmployeeMarks(int Maths ,int Physics) {	
	
		this.Maths = Maths ;
		this.Physics = Physics ;
		
	}
	
	@Override
	public int compareTo(EmployeeMarks o) {
		// TODO Auto-generated method stub
	// print Desc	
		return o.Maths - this.Maths ;
	}
	
	
	@Override
	public String toString() {
		return "EmployeeMarks [Maths=" + Maths + ", Physics=" + Physics + "]";
	}

	
	
}

package p_List;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class ArrayListTest {

public static void main(String[] args) {

	
	List<Integer> list = new ArrayList<Integer>();
	
	list.add(1);
	list.add(2);
	list.add(3);
	list.add(4);
	
//it is print collection Array format	
//	System.out.println(list);
	
// print collection object to array	
//Integer[] ar = list.toArray(new Integer[0]);	
//	
//	for(int x : ar) {
//		System.out.print(x);
//	}
		
      list.set(0, 100);	
      System.out.println(list);
	
	List<Integer> ar = new LinkedList<Integer>(list);
	
	ar.add(4);
	ar.add(5);
	ar.add(6);
	ar.add(7);
	
	ar.addAll(list);
	
List<Integer> arr = ar.subList(1, 4);

	System.out.println(arr);
		
		
	ListIterator<Integer> al = ar.listIterator();
	
	System.out.println(al.next());
	
	System.out.println(al.next());
	
	System.out.println(al.previous());
	
	}
}

package singletonpattern;

public class TVSet {

	private static TVSet tvSetInstance = null ;
	
	private TVSet() {

		System.out.println("TV is initiated");
    }	
	
	
	public static TVSet getTVSetInstance() {
		
		if(tvSetInstance == null) {
			synchronized(TVSet.class) {
				if(tvSetInstance == null) {
					tvSetInstance = new TVSet();
				}
			}
			
		}
		
		return tvSetInstance ;
	}
	
	    
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

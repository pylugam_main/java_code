package singletonpattern;

public class Driver {

	public static void main(String[] args) {
		
		TVSet t = TVSet.getTVSetInstance();
		
		TVSet t1 = TVSet.getTVSetInstance();
		
		System.out.println(t);
		
		System.out.println(t1);
		
	}
	
}

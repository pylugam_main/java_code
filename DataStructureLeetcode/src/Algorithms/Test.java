package Algorithms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.PriorityQueue;

public class Test {

	  static void sortByFrequency(int arr[]) {
	        // Map to store frequency of elements
	        LinkedHashMap<Integer, Integer> freqMap = new LinkedHashMap<>();
	        for (int num : arr) {
	            freqMap.put(num, freqMap.getOrDefault(num, 0) + 1);
	        }

	        // List to store the elements based on frequency and order of appearance
	        List<Integer> list = new ArrayList<>(freqMap.keySet());
	        
	        // Custom comparator to sort the list by frequency and then by order of appearance
	        Collections.sort(list, (a, b) -> {
	            int freqCompare = freqMap.get(b).compareTo(freqMap.get(a));
	            return (freqCompare == 0) ? (findFirstOccurrence(arr, a) - findFirstOccurrence(arr, b)) : freqCompare;
	        });

	        // Output the sorted array based on frequency and order of appearance
	        for (int num : list) {
	            for (int i = 0; i < freqMap.get(num); i++) {
	                System.out.print(num + " ");
	            }
	        }
	    }

	    // Function to find the first occurrence of an element in the array
	    static int findFirstOccurrence(int[] arr, int element) {
	        for (int i = 0; i < arr.length; i++) {
	            if (arr[i] == element) {
	                return i;
	            }
	        }
	        return -1;
	    }

	  
	    public static void sortColors(int[] arr) {
	        int low = 0;        // Pointer for the next 0
	        int mid = 0;        // Pointer for the current element
	        int high = arr.length - 1;  // Pointer for the next 2

	        while (mid <= high) {
	            switch (arr[mid]) {
	                case 0:
	                    // Swap arr[low] and arr[mid], then increment both low and mid
	                    swap(arr, low, mid);
	                    low++;
	                    mid++;
	                    break;
	                case 1:
	                    // 1 is in the correct place, just move mid
	                    mid++;
	                    break;
	                case 2:
	                    // Swap arr[mid] and arr[high], then decrement high
	                    swap(arr, mid, high);
	                    high--;
	                    break;
	            }
	        }
	    }

	    // Helper function to swap elements at two indices
	    private static void swap(int[] arr, int i, int j) {
	        int temp = arr[i];
	        arr[i] = arr[j];
	        arr[j] = temp;
	    }

	   
	    // Class to represent an element in the min-heap
	    static class Element {
	        int value;  // Value of the element
	        int machineIndex;  // Index of the machine from which the element came
	        int indexInMachine;  // Index of the element in its machine

	        Element(int value, int machineIndex, int indexInMachine) {
	            this.value = value;
	            this.machineIndex = machineIndex;
	            this.indexInMachine = indexInMachine;
	        }
	    }

	    public static List<Integer> mergeMachines(List<List<Integer>> machines) {
	        PriorityQueue<Element> minHeap = new PriorityQueue<>(Comparator.comparingInt(e -> e.value));
	        List<Integer> mergedList = new ArrayList<>();

	        // Initialize the heap with the first element from each machine
	        for (int i = 0; i < machines.size(); i++) {
	            List<Integer> machine = machines.get(i);
	            if (!machine.isEmpty()) {
	                minHeap.add(new Element(machine.get(0), i, 0));
	            }
	        }

	        // Extract the smallest element from the heap and add it to the result list
	        while (!minHeap.isEmpty()) {
	            Element minElement = minHeap.poll();
	            mergedList.add(minElement.value);

	            // Add the next element from the same machine to the heap
	            List<Integer> machine = machines.get(minElement.machineIndex);
	            if (minElement.indexInMachine + 1 < machine.size()) {
	                minHeap.add(new Element(machine.get(minElement.indexInMachine + 1), minElement.machineIndex, minElement.indexInMachine + 1));
	            }
	        }

	        return mergedList;
	        
	    }
	
	    
	    public static void sortInWave(int[] arr) {
	        // Sort the array
	        Arrays.sort(arr);
	        
	        // Swap adjacent elements to form the wave
	        for (int i = 0; i < arr.length - 1; i += 2) {
	            // Swap arr[i] with arr[i+1] if i+1 is within bounds
	            if (i + 1 < arr.length) {
	                swap1(arr, i, i + 1);
	            }
	        }
	    }

	    // Helper function to swap elements at two indices
	    private static void swap1(int[] arr, int i, int j) {
	        int temp = arr[i];
	        arr[i] = arr[j];
	        arr[j] = temp;
	    }
	    
	    // Class to represent an interval
	    static class Interval {
	        int start;
	        int end;

	        Interval(int start, int end) {
	            this.start = start;
	            this.end = end;
	        }
	    }

	    public static boolean hasIntersection(Interval[] intervals) {
	        // Sort intervals by start time
	        Arrays.sort(intervals, Comparator.comparingInt(i -> i.start));

	        // Check for intersections
	        for (int i = 0; i < intervals.length - 1; i++) {
	            // If the end of the current interval is greater than the start of the next interval
	            if (intervals[i].end >= intervals[i + 1].start) {
	                return true; // There is an overlap
	            }
	        }
	        return false; // No overlap found
	    }

	    // Function to count inversions using merge sort
	    public static int countInversions(int[] arr) {
	        int[] tempArr = new int[arr.length];
	        return mergeSortAndCount(arr, tempArr, 0, arr.length - 1);
	    }

	    // Function to perform merge sort and count inversions
	    private static int mergeSortAndCount(int[] arr, int[] tempArr, int left, int right) {
	        int mid, invCount = 0;
	        if (left < right) {
	            mid = (left + right) / 2;

	            invCount += mergeSortAndCount(arr, tempArr, left, mid);
	            invCount += mergeSortAndCount(arr, tempArr, mid + 1, right);

	            invCount += mergeAndCount(arr, tempArr, left, mid, right);
	        }
	        return invCount;
	    }

	    // Function to merge two halves and count inversions
	    private static int mergeAndCount(int[] arr, int[] tempArr, int left, int mid, int right) {
	        int i = left;    // Starting index for left subarray
	        int j = mid + 1; // Starting index for right subarray
	        int k = left;    // Starting index to be sorted
	        int invCount = 0;

	        // Merge the two subarrays into tempArr[]
	        while (i <= mid && j <= right) {
	            if (arr[i] <= arr[j]) {
	                tempArr[k++] = arr[i++];
	            } else {
	                tempArr[k++] = arr[j++];
	                invCount += (mid - i + 1); // Count inversions
	            }
	        }

	        // Copy remaining elements of left subarray, if any
	        while (i <= mid) {
	            tempArr[k++] = arr[i++];
	        }

	        // Copy remaining elements of right subarray, if any
	        while (j <= right) {
	            tempArr[k++] = arr[j++];
	        }

	        // Copy the sorted subarray into Original array
	        for (i = left; i <= right; i++) {
	            arr[i] = tempArr[i];
	        }

	        return invCount;
	    }

	    public static int[] findUnsortedSubarray(int[] arr) {
	        int n = arr.length;
	        int start = 0, end = n - 1;

	        // Step 1: Find the first out-of-order element from the beginning
	        while (start < n - 1 && arr[start] <= arr[start + 1]) {
	            start++;
	        }
	        
	        // If the array is already sorted
	        if (start == n - 1) {
	            return new int[] {-1, -1}; // No unsorted subarray
	        }

	        // Step 2: Find the first out-of-order element from the end
	        while (end > 0 && arr[end] >= arr[end - 1]) {
	            end--;
	        }

	        // Step 3: Find the minimum and maximum values in the identified subarray
	        int min = Integer.MAX_VALUE;
	        int max = Integer.MIN_VALUE;

	        for (int i = start; i <= end; i++) {
	            if (arr[i] < min) min = arr[i];
	            if (arr[i] > max) max = arr[i];
	        }

	        // Step 4: Expand the subarray to include any elements which are out of place
	        while (start > 0 && arr[start - 1] > min) {
	            start--;
	        }
	        while (end < n - 1 && arr[end + 1] < max) {
	            end++;
	        }

	        return new int[] {start, end};
	    }
   
	    
	    // Function to sort a nearly sorted array
	    public static void sortKSortedArray(int[] arr, int k) {
	        // Create a min-heap (priority queue)
	        PriorityQueue<Integer> minHeap = new PriorityQueue<>();
	        int n = arr.length;

	        // Build the initial heap with the first k+1 elements
	        for (int i = 0; i <= k && i < n; i++) {
	            minHeap.add(arr[i]);
	        }

	        // Process the remaining elements
	        int index = 0;
	        for (int i = k + 1; i < n; i++) {
	            // Extract the minimum element from the heap and put it in the sorted position
	            arr[index++] = minHeap.poll();
	            // Add the current element to the heap
	            minHeap.add(arr[i]);
	        }

	        // Extract the remaining elements from the heap
	        while (!minHeap.isEmpty()) {
	            arr[index++] = minHeap.poll();
	        }
	    }

  
    
}

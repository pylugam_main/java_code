package Algorithms;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

import Algorithms.Test.Interval;

import java.util.*;

public class Main {

	  public static void main(String[] args) {
		  
//	        int arr1[] = {2, 5, 2, 8, 5, 6, 8, 8};
//	        int arr2[] = {2, 5, 2, 6, -1, 9999999, 5, 8, 8, 8};
//
//	        System.out.print("Output for arr1: ");
//	        Test.sortByFrequency(arr1);
//	        
//	        System.out.println();
//	        
//	        System.out.print("Output for arr2: ");
//	        Test.sortByFrequency(arr2);

	  	  
//		        int[] arr1 = {0, 1, 2, 0, 1, 2};
//		        int[] arr2 = {0, 1, 1, 0, 1, 2, 1, 2, 0, 0, 0, 1};
//
//		        System.out.print("Output for arr1: ");
//		        Test.sortColors(arr1);
//		        printArray(arr1);
//
//		        System.out.print("Output for arr2: ");
//		        Test.sortColors(arr2);
//		        printArray(arr2);
//		        
		  
//		        List<Integer> machine1 = Arrays.asList(30, 40, 50);
//		        List<Integer> machine2 = Arrays.asList(35, 45);
//		        List<Integer> machine3 = Arrays.asList(10, 60, 70, 80, 100);
//
//		        List<List<Integer>> machines = Arrays.asList(machine1, machine2, machine3);
//
//		        List<Integer> sortedNumbers = Test.mergeMachines(machines);
//		        System.out.println("Output: " + sortedNumbers);
//		        
	      
//		  Interval[] intervals1 = { new Interval(1, 3), new Interval(5, 7), new Interval(2, 4), new Interval(6, 8) };
//	        Interval[] intervals2 = { new Interval(1, 3), new Interval(7, 9), new Interval(4, 6), new Interval(10, 13) };
//
//	System.out.println("Output for intervals1: "
//	                        + Test.hasIntersection(intervals1));
//	        System.out.println("Output for intervals2: "
//	                           + Test.hasIntersection(intervals2));
	        
	        
//	        int[] arr = {4, 3, 2, 1};
//	   System.out.println("Number of inversions: " + Test.countInversions(arr));
//   
//       int[] arr1 = {10, 12, 20, 30, 25, 40, 32, 31, 35, 50, 60};
//       int[] arr2 = {0, 1, 15, 25, 6, 7, 30, 40, 50};
//
//       int[] result1 = Test.findUnsortedSubarray(arr1);
//       int[] result2 = Test.findUnsortedSubarray(arr2);
//
//       System.out.println("Output for arr1: [" + result1[0] + ", " + result1[1] + "]");
//       System.out.println("Output for arr2: [" + result2[0] + ", " + result2[1] + "]");
//
	   
       int[] arr1 = {6, 5, 3, 2, 8, 10, 9};
       int k1 = 3;
       Test.sortKSortedArray(arr1, k1);
       System.out.println("Sorted array: " + java.util.Arrays.toString(arr1));

       int[] arr2 = {10, 9, 8, 7, 4, 70, 60, 50};
       int k2 = 4;
       Test.sortKSortedArray(arr2, k2);
       System.out.println("Sorted array: " + java.util.Arrays.toString(arr2)); 
       
	  }
	  
	  // Helper function to print the array
	    private static void printArray(int[] arr) {
	        for (int num : arr) {
	            System.out.print(num + " ");
	        }
	        System.out.println();
	    }

	  
	    // Class to represent an element in the min-heap	    
	  
}

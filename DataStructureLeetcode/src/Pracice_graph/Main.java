package Pracice_graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import leetlinkedlist.LinkedList;

public class Main {

	 public static void main(String[] args) {
		 
		 
               Solution solution = new Solution();
//	        
//	        // Example 1
//	        int numCourses1 = 2;
//	        int[][] prerequisites1 = {{1, 0}};
//	      System.out.println( solution.canFinish(numCourses1, prerequisites1)); // Output: true
//
	     //1
		        // Test Example 1
//		        Node node1 = new Node(1);
//		        Node node2 = new Node(2);
//		        Node node3 = new Node(3);
//		        Node node4 = new Node(4);
//		        
//		        node1.neighbors.add(node2);
//		        node1.neighbors.add(node4);
//		        
//		        node2.neighbors.add(node1);
//		        node2.neighbors.add(node3);
//		        
//		        node3.neighbors.add(node2);
//		        node3.neighbors.add(node4);
//		        
//		        node4.neighbors.add(node1);
//		        node4.neighbors.add(node3);
//		        
//		        Node clone = solution.cloneGraph(node1);
//		        
//		        // Print out the clone to verify it's correct
//		        Main.printGraph(clone);
		   
               //3
		        // Example 1
//		        int[][] heights1 = {
//		            {1, 2, 2, 3, 5},
//		            {3, 2, 3, 4, 4},
//		            {2, 4, 5, 3, 1},
//		            {6, 7, 1, 4, 5},
//		            {5, 1, 1, 2, 4}
//		        };
//		        List<List<Integer>> result1 = solution.pacificAtlantic(heights1);
//		        System.out.println("Example 1 Output: " + result1);
//		        
//		        // Example 2
//		        int[][] heights2 = {
//		            {1}
//		        };
//		        List<List<Integer>> result2 = solution.pacificAtlantic(heights2);
//		        System.out.println("Example 2 Output: " + result2);

		        //4
//               char[][] grid1 = {
//                       {'1', '1', '1', '1', '0'},
//                       {'1', '1', '0', '1', '0'},
//                       {'1', '1', '0', '0', '0'},
//                       {'0', '0', '0', '0', '0'}
//                   };
//                   System.out.println("Example 1 Output: " + solution.numIslands(grid1)); // Output: 1
//                   
//                   // Example 2
//                   char[][] grid2 = {
//                       {'1', '1', '0', '0', '0'},
//                       {'1', '1', '0', '0', '0'},
//                       {'0', '0', '1', '0', '0'},
//                       {'0', '0', '0', '1', '1'}
//                   };
//                   System.out.println("Example 2 Output: " + solution.numIslands(grid2)); 		        
//		        
		        // 5
               
//                   int[] nums1 = {100, 4, 200, 1, 3, 2};
//                   System.out.println("Example 1 Output: " + solution.longestConsecutive(nums1)); // Output: 4
//                   
//                   // Example 2
//                   int[] nums2 = {0, 3, 7, 2, 5, 8, 4, 6, 0, 1};
//                   System.out.println("Example 2 Output: " + solution.longestConsecutive(nums2)); // Output: 9
        
		        
		        
		        
		        
		        
		        
		    // Helper function to print out the graph in adjacency list format
		   
	 }
	 
	 public static void printGraph(Node node) {
	        if (node == null) return;
	        
	        HashSet<Node> visited = new HashSet<>();
	        Queue<Node> queue = new java.util.LinkedList<>();
	        queue.add(node);
	        visited.add(node);
	        
	        while (!queue.isEmpty()) {
	            Node n = queue.poll();
	            System.out.print("Node " + n.val + " neighbors: ");
	            for (Node neighbor : n.neighbors) {
	                System.out.print(neighbor.val + " ");
	                if (!visited.contains(neighbor)) {
	                    visited.add(neighbor);
	                    queue.add(neighbor);
	                }
	            }
	            System.out.println();
	        }
	    }	 
}
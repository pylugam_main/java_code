package Pracice_graph;
import java.util.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;

public class Solution {

	 public boolean canFinish(int numCourses, int[][] prerequisites) {
	        // Create an adjacency list for the graph
	        List<Integer>[] graph = new ArrayList[numCourses];
	        for (int i = 0; i < numCourses; i++) {
	            graph[i] = new ArrayList<>();
	        }
	        
	        // Populate the graph with the prerequisites
	        for (int[] prerequisite : prerequisites) {
	            graph[prerequisite[1]].add(prerequisite[0]);
	        }
	        
	        // Array to track the visited status of each node
	        int[] visited = new int[numCourses];
	        
	        // Check each course (node) to see if we can finish it
	        for (int i = 0; i < numCourses; i++) {
	            if (hasCycle(graph, visited, i)) {
	                return false;
	            }
	        }
	        
	        return true;
	    }
	    
	    // DFS helper function to detect a cycle
	    private boolean hasCycle(List<Integer>[] graph, int[] visited, int course) {
	        if (visited[course] == 1) {
	            // Found a cycle
	            return true;
	        }
	        if (visited[course] == 2) {
	            // Already processed this node, no cycle found earlier
	            return false;
	        }
	        
	        // Mark the node as visiting
	        visited[course] = 1;
	        
	        // Perform DFS on all neighbors
	        for (int neighbor : graph[course]) {
	            if (hasCycle(graph, visited, neighbor)) {
	                return true;
	            }
	        }
	        
	        // Mark the node as visited
	        visited[course] = 2;
	        return false;
	    }

	    // HashMap to store visited nodes and their clones
	    private HashMap<Node, Node> visited = new HashMap<>();
	    
	    public Node cloneGraph(Node node) {
	        if (node == null) {
	            return null;
	        }
	        
	        // If the node has already been visited, return its clone from the map
	        if (visited.containsKey(node)) {
	            return visited.get(node);
	        }
	        
	        // Clone the node (but not its neighbors yet)
	        Node cloneNode = new Node(node.val);
	        visited.put(node, cloneNode);
	        
	        // Recursively clone all the neighbors
	        for (Node neighbor : node.neighbors) {
	            cloneNode.neighbors.add(cloneGraph(neighbor));
	        }
	        
	        return cloneNode;
	    }
	    public List<List<Integer>> pacificAtlantic(int[][] heights) {
	        List<List<Integer>> result = new ArrayList<>();
	        if (heights == null || heights.length == 0 || heights[0].length == 0) {
	            return result;
	        }
	        
	        int m = heights.length;
	        int n = heights[0].length;
	        boolean[][] pacific = new boolean[m][n];
	        boolean[][] atlantic = new boolean[m][n];
	        
	        // Run DFS from all cells adjacent to the Pacific and Atlantic oceans
	        for (int i = 0; i < m; i++) {
	            dfs(heights, pacific, i, 0);
	            dfs(heights, atlantic, i, n - 1);
	        }
	        for (int i = 0; i < n; i++) {
	            dfs(heights, pacific, 0, i);
	            dfs(heights, atlantic, m - 1, i);
	        }
	        
	        // Collect cells that can reach both oceans
	        for (int i = 0; i < m; i++) {
	            for (int j = 0; j < n; j++) {
	                if (pacific[i][j] && atlantic[i][j]) {
	                    result.add(Arrays.asList(i, j));
	                }
	            }
	        }
	        
	        return result;
	    }
	    
	    private void dfs(int[][] heights, boolean[][] ocean, int i, int j) {
	        ocean[i][j] = true;
	        int m = heights.length;
	        int n = heights[0].length;
	        int[][] directions = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
	        
	        for (int[] dir : directions) {
	            int x = i + dir[0];
	            int y = j + dir[1];
	            if (x >= 0 && x < m && y >= 0 && y < n && !ocean[x][y] && heights[x][y] >= heights[i][j]) {
	                dfs(heights, ocean, x, y);
	            }
	        }

	    }
	    
	    
	    
	    public int numIslands(char[][] grid) {
	        if (grid == null || grid.length == 0) {
	            return 0;
	        }
	        
	        int numIslands = 0;
	        int m = grid.length;
	        int n = grid[0].length;
	        
	        for (int i = 0; i < m; i++) {
	            for (int j = 0; j < n; j++) {
	                if (grid[i][j] == '1') {
	                    numIslands++;
	                    dfs(grid, i, j);
	                }
	            }
	        }
	        
	        return numIslands;
	    }
	    
	    private void dfs(char[][] grid, int i, int j) {
	        int m = grid.length;
	        int n = grid[0].length;
	        
	        // Check if the current cell is out of bounds or not land
	        if (i < 0 || i >= m || j < 0 || j >= n || grid[i][j] == '0') {
	            return;
	        }
	        
	        // Mark the cell as visited by setting it to '0'
	        grid[i][j] = '0';
	        
	        // Explore all four directions
	        dfs(grid, i + 1, j); // down
	        dfs(grid, i - 1, j); // up
	        dfs(grid, i, j + 1); // right
	        dfs(grid, i, j - 1); // left
	    }	
	    
	    
	    public int longestConsecutive(int[] nums) {
	        if (nums == null || nums.length == 0) {
	            return 0;
	        }

	        Set<Integer> numSet = new HashSet<>();
	        for (int num : nums) {
	            numSet.add(num);
	        }

	        int longestStreak = 0;

	        for (int num : numSet) {
	            // Only start counting if `num` is the start of a sequence
	            if (!numSet.contains(num - 1)) {
	                int currentNum = num;
	                int currentStreak = 1;

	                // Count all the consecutive numbers
	                while (numSet.contains(currentNum + 1)) {
	                    currentNum += 1;
	                    currentStreak += 1;
	                }

	                // Update the maximum streak length
	                longestStreak = Math.max(longestStreak, currentStreak);
	            }
	        }

	        return longestStreak;
	    }

}

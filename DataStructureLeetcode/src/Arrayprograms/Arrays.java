package Arrayprograms;

import java.util.HashMap;

public class Arrays {

//check this method working or not 	
public void  firstRepeating(int[] numbers) {
	
	int n = numbers.length ;
	HashMap<Integer, Integer> list = new HashMap<Integer, Integer>();
	int check = Integer.MAX_VALUE ;
	for(int num = 0 ; num < numbers.length ; num++) {
		if(list.containsKey(numbers[num])) {
			check = Math.min(check, list.get(numbers[num]));
		}
		else {
			list.put(numbers[num],num + 1);
		}
	}
	if(check == Integer.MAX_VALUE) {
		System.out.print("-1");
	}
	else {
		System.out.print(check);
	}
	
}
	

// this method shows non repeat element	
public void nonrepeat(int[] numbers) {
	
	HashMap<Integer, Integer> list = new HashMap<Integer, Integer>();
	for(int start = 0 ; start < numbers.length ; start++) {
         list.put(numbers[start], list.getOrDefault(numbers[start], 0) + 1) ;		
		}
	for(int start = 0 ; start<numbers.length ; start++) {
		if(list.get(numbers[start]) == 1) {
			System.out.println(numbers[start]);
		}
	}
	System.out.println("-1");
		
}		
			

//rotate array 51234			
public void rotate(int[] arr) {
	
	int n = arr.length ;
	int temp = arr[n - 1] ;
	
for(int i = n- 1 ; i > 0 ; i--) {
	arr[i] = arr[i - 1] ;
}	
    arr[0] = temp ;	

    for(int num : arr) {
    	System.out.print(num+" ");
    }
    
}			
	
		
//		
	
	
	

	
	public static void main(String[] args) {
		
		Arrays rank = new Arrays();
		
		int[] numbers = {1,2,3,4,5,6,7,8,8};
		
		rank.firstRepeating(numbers);
		
		int[] numbers1 = {1,2,3,4,5,6,7,8,8};

		rank.nonrepeat(numbers1);
	
	    int[] rotate = {1,2,3,4,5};
	
	    rank.rotate(rotate) ;
	
	}
	 
	
}

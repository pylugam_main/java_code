package stringprograms;

import java.util.Arrays;
import java.util.HashMap;

public class Demo_String {

	
	public void StringReverse() {
		
		 String name = " i.like.this.program.very.much";
		 
		 String[] words  = name.split("\\.") ;
		 
		 StringBuilder build = new StringBuilder();
		 
		 for(int i = words.length -1 ; i >=0 ; i-- ) {
			 
			 build.append(words[i]) ;
			 
			 if(i != 0 ) {
				 
				 build.append(".") ;
			 }
		 }
		 
		 System.out.println(build.toString());
		 
	}
	
	
public String longestcommenprefix(String[] arr ,int n ) {
	
	if(n == 0) {
		return " " ;
	}
	
	String prefix = arr[0] ;
	
	for(int words = 0 ; words < arr.length ; words++) {
		
		while(arr[words].indexOf(prefix) != 0) {
			
			prefix = prefix.substring(0 , prefix.length()-1) ;

		;
			if(prefix.isEmpty()) {
				
				return "-1";
			}
		}
		
	}
	
	
	return prefix ;
	
	
}	
	
	
public void RomanToInteger(String str) {
	
	HashMap<Character, Integer> map = new HashMap<Character, Integer>();
	
	map.put('I', 1) ;
	
	map.put('V', 5) ;
	
	map.put('X', 10) ;
	
	map.put('L', 50);
	
	map.put('C',100);
	
	map.put('D', 500);
	
	map.put('M', 1000); 
	
	int total = 0 ;
	
	int prevalue = 0 ;
	
	for(int start = str.length() - 1 ; start >= 0 ; start--) {
		
	int box = map.get(str.charAt(start)) ;
	
	if(box < prevalue ) {
		
		total -= box ; 
	}
	else {
		
		total += box ;
	}
	
	prevalue = box ;
	
	}
	
	System.out.println(total);
}


public void IntToRoman(int n) {
	
	String[] symbol = {"M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I"};
	
	int[] values = {1000,900,500,400,100,90,50,40,10,9,5,4,1};
	
	int i = 0 ;
	
	StringBuilder sb = new StringBuilder();
	
	while(n > 0) {
		
		while(n >= values[i]) {
			
			n -= values[i] ;
			
			sb.append(symbol[i]) ;
		}
		
		i++ ;
	}
	
	System.out.println(sb);
}

public void mindistance(String[] str , String word1, String word2) {
	
	int mindistance = Integer.MAX_VALUE ;
	
	int index1 = -1 ;
	
	int index2 = -1 ;
	
	for(int words =  0 ; words < str.length ; words++) {
		
		if(str[words].equals(word1)) {
			
			index1 = words ;
			
			if(index2 != -1 )
		mindistance = Math.min(mindistance, Math.abs(index1 - index2)) ;
		
		}
		else if(str[words].equals(word2)) {
			
			index2 = words ;
			
			if(index1 != -1) {
				
				mindistance = Math.min(mindistance, Math.abs(index1 - index2));
			}
		}
	}
	
	System.out.println(mindistance);
}


public void MINMAX(int[] value) {
	
	int min = Integer.MAX_VALUE ;
			
	int max  = Integer.MIN_VALUE ;
	
	for(int num = 0 ; num < value.length ; num++) {
		
		if(value[num] < min ) {
			
			min = value[num] ;
		}
		else if(value[num] > max) {
			
			max = value[num] ;
		}
	}
	
	
	System.out.println(min);
	
	System.out.println(max);
		
}


public void peak(int[] peak) {
	
	int left = 0 ;
	
	int right = peak.length - 1 ;
	
	while(left < right) {
		
		int mid = left - (left - right) / 2 ;
		
		if(peak[mid] > peak[mid + 1]) {
			
			right = mid ;
		}
		else {
			
			left = mid + 1 ;
		}
		
	}
	
	System.out.println(left);
	
	
}
	
public void reverseString(String name) {
	
   char[] names =name.toCharArray(); 	
	
   char[] word = new char[name.length()] ; 

   int iterater = 0 ;
   
for(int start = names.length - 1 ; start >= 0 ; start--) {
	
	word[iterater] = names[start] ;
	
	     iterater++ ;
	
}

String k = new String(word);

System.out.println(k);


}


public void sort(int[] arr) {
	
 	 for(int i = 0 ; i < arr.length ; i++) {
 		 
 		 for(int j = i + 1 ; j < arr.length ; j++) {
 			 
 			 if(arr[i] > arr[j]) {
 				 
 				 int temp = arr[i] ;
 				 
 				 arr[i] = arr[j] ;
 				 
 				 arr[j] = temp ;
 			 }
 		 }
 		 
 	 }
	
	
 	 
	for(int i = 0 ; i < arr.length ; i++) {
		
		System.out.print(arr[i]);
	}
	
	
}


public void sortnegative(int[] negative) {
	
	
	for(int i = 0 ; i < negative.length ; i++) {
		
		int j = 0 ;
		
		if(negative[i] >= 0 ) {
			
			if( i != j) {
		
				int temp = negative[i] ;
				
				negative[i] = negative[j];
				
				negative[j] = temp ;
			}
			
			j++ ;
		}
		
		
	}
	
	
	
	for(int i = 0 ; i < negative.length ; i++) {
		
		System.out.print(negative[i]);
	}
	
	
		
	
}


	public static void main(String[] args) {
		
		Demo_String str = new Demo_String();
		
		str.StringReverse();
		
	String [] arr = {"geeks","gee"}; 	
		
		int n = arr.length ;
		
		System.out.println(str.longestcommenprefix(arr, n));
	
		str.RomanToInteger("III");
		
		str.IntToRoman(10);

		String[] name = {"this","is","kannan","welcome"};
		
		String word1 = "this" ; 
		
		String word2 = "welcome" ;
		
	//	str.mindistance(name, word1, word2);
	
	//	int[] value = {1,2,3,4,56,7};
		
	//	str.MINMAX(value);

		
		int[] peak = {1,2,3,4,56,7};
	
		str.peak(peak);

		String m = "kannan" ;
		
		str.reverseString(m);
		
		int[] arr1 = {7,7,8,2,9,6,5,4}; 
		
	//	str.sort(arr1);
		
		
int[] negative = {1,6,8,4,3,-2,-8,-7};
	
	str.sortnegative(negative);
	
	
	
	}
	
	
	 
	
}

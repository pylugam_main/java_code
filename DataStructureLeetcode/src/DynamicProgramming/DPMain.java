package DynamicProgramming;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DPMain  {

	
public int climbStairs(int n) {
        if (n == 0 || n == 1) {	
            return 1;
        }
        int prev = 1, curr = 1;
        for (int i = 2; i <= n; i++) {
            int temp = curr;
            curr = prev + curr;
            prev = temp;
        }
        return curr;
    }	
	
	
public int coinChange(int[] coins, int amount) {
    int[] minCoins = new int[amount + 1];
    Arrays.fill(minCoins, amount + 1);
    minCoins[0] = 0;

    for (int i = 1; i <= amount; i++) {
        for (int j = 0; j < coins.length; j++) {
            if (i - coins[j] >= 0) {
                minCoins[i] = Math.min(minCoins[i], 1 + minCoins[i - coins[j]]);
            }
        }
    }

    return minCoins[amount] != amount + 1 ? minCoins[amount] : -1;        
}	
	
	

public int lengthOfLIS(int[] nums) {
    if (nums == null || nums.length == 0) {
        return 0;
    }

    int n = nums.length;
    int[] dp = new int[n];
    Arrays.fill(dp, 1);

    for (int i = 1; i < n; ++i) {
        for (int j = 0; j < i; ++j) {
            if (nums[i] > nums[j]) {
                dp[i] = Math.max(dp[i], dp[j] + 1);
            }
        }
    }

    int maxLength = Arrays.stream(dp).max().orElse(0);
    return maxLength;
}	



public int longestCommonSubsequence(String text1, String text2) {
    int n = text1.length();
    int m = text2.length();
    int[][] dpGrid = new int[n + 1][m + 1];

    for (int row = n - 1; row >= 0; row--) {
        for (int col = m - 1; col >= 0; col--) {
            if (text1.charAt(row) == text2.charAt(col)) {
                dpGrid[row][col] = 1 + dpGrid[row + 1][col + 1];
            } else {
                dpGrid[row][col] = Math.max(dpGrid[row + 1][col], dpGrid[row][col + 1]);
            }
        }
    }
    return dpGrid[0][0];
}


private Map<String, Boolean> memo = new HashMap<>();

public boolean helper(String s,Set<String> wordSet){
    if (memo.containsKey(s)) {
        return memo.get(s);
    }
    if(s.length()==0)return true;
    
    for(int j = 1;j<=s.length();j++){
         String substring = s.substring(0, j);
         if(wordSet.contains(substring)){
            memo.put(substring, true);
            String rSubstring = s.substring(j);
             if (helper(rSubstring, wordSet)) {
                return true;
            }
         }
    }
    memo.put(s, false);
    return false;
}
public boolean wordBreak(String s, List<String> wordDict) {
    Set<String> wordSet = new HashSet<>(wordDict);
    return helper(s,wordSet);
    
}



public int combinationSum4(int[] nums, int target) {
    int[]dp=new int[target+1];
    Arrays.fill(dp,-1);
    return helper(target, nums,dp);
}

private int helper(int target, int[] nums,int[] dp) {
    if (target < 0) {
        return 0;
    }
    if (target == 0) {
        return 1;
    }
    if(dp[target]!=-1){
        return dp[target];
    }
    int c = 0;
    for (int num : nums) {
        c += helper(target - num, nums,dp);
    }
    return dp[target]=c;
}


public int rob(int[] nums) {
    int n = nums.length;

    if (n == 1) {
        return nums[0];
    }

    int[] dp = new int[n];

    dp[0] = nums[0];
    dp[1] = Math.max(nums[0], nums[1]);

    for (int i = 2; i < n; i++) {
        dp[i] = Math.max(dp[i - 1], nums[i] + dp[i - 2]);
    }

    return dp[n - 1];        
}


public int rob1(int[] nums) {
    if (nums.length == 1) return nums[0];
    return Math.max(getMax(nums, 0, nums.length - 2), getMax(nums, 1, nums.length - 1));        
}

private int getMax(int[] nums, int start, int end) {
    int prevRob = 0, maxRob = 0;

    for (int i = start; i <= end; i++) {
        int temp = Math.max(maxRob, prevRob + nums[i]);
        prevRob = maxRob;
        maxRob = temp;
    }

    return maxRob;
}    

	
public int numDecodings(String s) {
    int strLen = s.length();

    int[] dp = new int[strLen + 1];
    // there is only one way to decode an empty string
    dp[0] = 1;

    // the first element of the dp array is 1 if the first
    // character of the string is not '0',
    if (s.charAt(0) != '0') {
        dp[1] = 1;
    } else {
        // there's no way to decode a string that starts
        // with '0'
        return 0;
    }

    // iterate through the input string starting from the
    // 2nd character
    for (int i = 2; i <= strLen; ++i) {
        // if the current character is not '0', add the
        // number of ways to decode the substring without
        // the current character
        if (s.charAt(i - 1) != '0') {
            dp[i] += dp[i - 1];
        }

        // if the substring of the current and previous
        // characters is a valid two-digit number, add the
        // number of ways to decode the substring without
        // the current and previous characters
        if (s.charAt(i - 2) == '1' ||
                (s.charAt(i - 2) == '2' && s.charAt(i - 1) <= '6')) {
            dp[i] += dp[i - 2];
        }
    }

    return dp[strLen];
}


public int uniquePaths(int m, int n) {
    //Covers two trivial cases & saves some time
    if (m == 1 || n == 1) {
        return 1;
    }

    //Initialize DP Matrix
    int[][] opt = new int[m][n];

    
    //Recurrence – Populate table with Base Case 1
    for (int i = 0; i < m; i++) {
        opt[i][n-1] = 1;
    }

    //Recurrence – Populate table with Base Case 2
    for (int i = 0; i < n; i++) {
        opt[m-1][i] = 1;
    }

    int row = m-2;
    int col = n-2;

    //As we populate the table, reference already computed values
    while(row >= 0 && col >= 0) {
        //Compute all cells in row to the left of the current cell
        for (int i = col; i >= 0; i--) {
            opt[row][i] = opt[row+1][i] + opt[row][i+1];
        }

        //Compute all cells in col above the current cell
        for (int j = row; j >= 0; j--) {
            opt[j][col] = opt[j][col+1] + opt[j+1][col];
        }

        //Traverse upwards and to the left, diagonally
        row --;
        col --;
    }

    //From our recurrence – how many paths from (0, 0) to (m, n)?
    return opt[0][0];
}


public boolean canJump(int[] nums) {
    int goal = nums.length - 1;

    for (int i = nums.length - 2; i >= 0; i--) {
        if (i + nums[i] >= goal) {
            goal = i;
        }
    }

    return goal == 0;        
}



}

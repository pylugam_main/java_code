package java_Logical;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.PriorityQueue;

class Task{
	    public String longestDiverseString(int a, int b, int c) {
	        StringBuilder res = new StringBuilder();
	        PriorityQueue<int[]> maxHeap = new PriorityQueue<>((x, y) -> y[0] - x[0]);
	        
	        if (a > 0) maxHeap.offer(new int[]{a, 'a'});
	        if (b > 0) maxHeap.offer(new int[]{b, 'b'});
	        if (c > 0) maxHeap.offer(new int[]{c, 'c'});

	        while (!maxHeap.isEmpty()) {
	            int[] current = maxHeap.poll();
	            int count = current[0];
	            char char1 = (char) current[1];

	            if (res.length() > 1 && res.charAt(res.length() - 1) == char1 && res.charAt(res.length() - 2) == char1) {
	                if (maxHeap.isEmpty()) {
	                    break;
	                }
	                int[] next = maxHeap.poll();
	                res.append((char) next[1]);
	                next[0]--;
	                if (next[0] > 0) {
	                    maxHeap.offer(next);
	                }
	                current[0]--;
	            }

	            res.append(char1);
	            count--;
	            if (count > 0) {
	                maxHeap.offer(new int[]{count, char1});
	            }
	        }

	        return res.toString();
	    }
	
	    
	
	static public String longest1DiverseString(int a, int b, int c) {

		StringBuilder builder = new StringBuilder();
		PriorityQueue<Pair> pq = new PriorityQueue<Pair>(
	(count1, count2) -> Integer.compare(count2.count, count1.count));

		if (a > 0)
			pq.add(new Pair('a', a));
		if (b > 0)
			pq.add(new Pair('b', b));
		if (c > 0)
			pq.add(new Pair('c', c));

		while (pq.size() > 1) {

			Pair pair_one = pq.poll();
			if (pair_one.count >= 2) {
				builder.append(pair_one.ch);
				builder.append(pair_one.ch);
				pair_one.count -= 2;
			} else {
				builder.append(pair_one.ch);
				pair_one.count -= 1;
			}

			Pair pair_two = pq.poll();
			if (pair_two.count >= 2 && pair_one.count < pair_two.count) {
				builder.append(pair_two.ch);
				builder.append(pair_two.ch);
				pair_two.count -= 2;
			} else {
				builder.append(pair_two.ch);
				pair_two.count -= 1;
			}

			if (pair_one.count > 0)
				pq.add(pair_one);
			if (pair_two.count > 0)
				pq.add(pair_two);
		}

		if (!pq.isEmpty()) {
			if (builder.charAt(builder.length() - 1) != pq.peek().ch) {
				if (pq.peek().count >= 2) {
					builder.append(pq.peek().ch);
					builder.append(pq.peek().ch);
				} else {
					builder.append(pq.peek().ch);
				}
			}
		}
		return builder.toString();
	}

	static class Pair {
		public Character ch;
		int count;

		public Pair(Character ch, int count) {
			this.ch = ch;
			this.count = count;
		}
	}
	
		
	    public int removeCovered1Intervals(int[][] intervals) {
	        Arrays.sort(intervals,(a,b)->a[0]==b[0]?b[1]-a[1]:a[0]-b[0]);

	        int idx=0;
	        int cnt=1;
	        for(int i=1;i<intervals.length;i++){
	            if(intervals[i][1]>intervals[idx][1]){
	                cnt++;
	                idx=i;
	            }
	        }
	        return cnt;
	    }
	
	 	        public int removeCoveredIntervals(int[][] intervals) {
	            Arrays.sort(intervals, (a, b) -> {
	                if (a[0] != b[0]) {
	                    return Integer.compare(a[0], b[0]);
	                } else {
	                    return Integer.compare(b[1], a[1]);
	                }
	            });

	            List<int[]> res = new ArrayList<>();
	            res.add(intervals[0]);

	            for (int i = 1; i < intervals.length; i++) {
	                int prevL = res.get(res.size() - 1)[0];
	                int prevR = res.get(res.size() - 1)[1];
	                int r = intervals[i][1];

	                if (prevL <= intervals[i][0] && prevR >= r) {
	                    continue;
	                }

	                res.add(intervals[i]);
	            }

	            return res.size();
	        }
	    

	 	        
public static void main(String[] args) {

	Task l = new Task();
	
	System.out.println(l.longestDiverseString(1, 1, 7));
	
	int[][] intervals = {{1,4},{3,6},{2,8}} ;
	
	   System.out.println( l.removeCovered1Intervals(intervals));
	    
	}
	
	
	
	
	
	
}

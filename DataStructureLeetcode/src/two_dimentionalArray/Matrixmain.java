package two_dimentionalArray;

import java.util.ArrayList;
import java.util.List;
import java.util.Arrays ;

public class Matrixmain {

		  public void setZeroes(int[][] mat) {
		        int rows = mat.length;
		        int cols = mat[0].length;
		        boolean fcol = false;
		        boolean frow = false;

		        // Check if there is a zero in the first column, set fcol to true.
		        for (int i = 0; i < rows; i++) {
		            if (mat[i][0] == 0) {
		                fcol = true;
		                break;
		            }
		        }

		        // Check if there is a zero in the first row, set frow to true.
		        for (int i = 0; i < cols; i++) {
		            if (mat[0][i] == 0) {
		                frow = true;
		                break;
		            }
		        }

		        // Check row elements (by ignoring the first row and first column). If zero is
		        // found,
		        // set the corresponding row's and column's first element to zero.
		        for (int i = 1; i < rows; i++) {
		            for (int j = 1; j < cols; j++) {
		                if (mat[i][j] == 0) {
		                    mat[0][j] = 0;
		                    mat[i][0] = 0;
		                }
		            }
		        }

		        // Check every row's first element starting from the second row.
		        // Set the complete row to zero if zero is found.
		        for (int i = 1; i < rows; i++) {
		            if (mat[i][0] == 0) {
		                Arrays.fill(mat[i], 0);
		            }
		        }

		        // Check every column's first element starting from the second column.
		        // Set the complete column to zero if zero is found.
		        for (int j = 1; j < cols; j++) {
		            if (mat[0][j] == 0) {
		                for (int i = 1; i < rows; i++) {
		                    mat[i][j] = 0;
		                }
		            }
		        }

		        // If fcol is true, set the first column to zero.
		        if (fcol) {
		            for (int i = 0; i < rows; i++) {
		                mat[i][0] = 0;
		            }
		        }

		        // If frow is true, set the first row to zero.
		        if (frow) {
		            Arrays.fill(mat[0], 0);
		        }
		    }
		
		
		  public List<Integer> spiralOrder(int[][] matrix) {
		        // Calculate the total number of rows and columns
		        int rows = matrix.length;
		        int cols = matrix[0].length;

		        // Set up pointers to traverse the matrix
		        int row = 0;
		        int col = -1;

		        // Set the initial direction to 1 for moving left to right
		        int direction = 1;

		        // Create an array to store the elements in spiral order
		        List<Integer> result = new ArrayList<>();

		        // Traverse the matrix in a spiral order
		        while (rows > 0 && cols > 0) {

		            // Move horizontally in one of two directions:
		            // 1. Left to right (if direction == 1)
		            // 2. Right to left (if direction == -1)
		            // Increment the col pointer to move horizontally
		            for (int i = 0; i < cols; i++) {
		                col += direction;
		                result.add(matrix[row][col]);
		            }
		            rows--;

		            // Move vertically in one of two directions:
		            // 1. Top to bottom (if direction == 1)
		            // 2. Bottom to top (if direction == -1)
		            // Increment the row pointer to move vertically
		            for (int i = 0; i < rows; i++) {
		                row += direction;
		                result.add(matrix[row][col]);
		            }
		            cols--;

		            // Flip the direction for the next traversal
		            direction *= -1;
		        }

		        return result;
		    }
		  
		  
		  public void rotate(int[][] matrix) {
			  
			  int edgeLength = matrix.length;

		        int top = 0;
		        int bottom = edgeLength - 1;

		        while (top < bottom) {
		            for (int col = 0; col < edgeLength; col++) {
		                int temp = matrix[top][col];
		                matrix[top][col] = matrix[bottom][col];
		                matrix[bottom][col] = temp;
		            }
		            top++;
		            bottom--;
		        }

		        for (int row = 0; row < edgeLength; row++) {
		            for (int col = row + 1; col < edgeLength; col++) {
		                int temp = matrix[row][col];
		                matrix[row][col] = matrix[col][row];
		                matrix[col][row] = temp;
		            }
		        }        
		    }
		  
		  
		  
		  public boolean exist(char[][] board, String word) {
		        int n = board.length;
		        int m = board[0].length;

		        for (int row = 0; row < n; row++) {
		            for (int col = 0; col < m; col++) {
		                if (depthFirstSearch(board, word, row, col, 0)) {
		                    return true;
		                }
		            }
		        }
		        return false;
		    }

		  
 private boolean depthFirstSearch(char[][] board, String word,
		                          int row, int col, int index) {
	if (index == word.length()) {
		
		            return true;
		        }
	
  if (row < 0 || row >= board.length || col < 0 || col >= board[0].length
		          || board[row][col] != word.charAt(index)) {
	  
		     return false;
		     
		        }
  
 char temp = board[row][col];
		board[row][col] = '*'; // Mark the cell as visited

	 // Explore the four neighboring directions:
	//	right, down, left, up
	int[][] offsets = { { 0, 1 }, { 1, 0 }, { 0, -1 }, { -1, 0 } };
	
for (int[] offset : offsets) {
	
int newRow = row + offset[0];
int newCol = col + offset[1];

if (depthFirstSearch(board, word, newRow, newCol, index + 1)) {
 return true;
		            }
		        }

 board[row][col] = temp; // Restore the cell's original value
 return false;
		    }
			
		
		
	}


package Tree_Practice;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class TreeClass {

	
	public boolean isSameTree(TreeNode p, TreeNode q) {
        // If both nodes are null, the trees are identical at this point
        if (p == null && q == null) {
            return true;
        }
        // If one of the nodes is null, the trees are not the same
        if (p == null || q == null) {
            return false;
        }
        // If the values of the nodes are different, the trees are not the same
        if (p.data != q.data) {
            return false;
        }
        // Recursively check the left and right subtrees
        return isSameTree(p.left, q.left) && isSameTree(p.right, q.right);
    }
	
	
	

    public TreeNode invertTree(TreeNode root) {
        if (root == null) {
            return null;
        }
        
        TreeNode temp = root.left;
        root.left = root.right;
        root.right = temp;
        
        invertTree(root.left);
        invertTree(root.right);
        
        return root;        
    }
	
    public static void printTree(TreeNode root) {
        if (root == null) {
            System.out.println("[]");
            return;
        }
        
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        while (!queue.isEmpty()) {
            TreeNode node = queue.poll();
            if (node != null) {
                System.out.print(node.data + " ");
                queue.add(node.left);
                queue.add(node.right);
            } else {
                System.out.print("null ");
            }
        }
        System.out.println();
    }
	
	//4
    
    private int maxSum = Integer.MIN_VALUE;

    public int maxPathSum(TreeNode root) {
        // Start the recursion to find the maximum path sum
        findMaxPath(root);
        return maxSum;
    }

    private int findMaxPath(TreeNode node) {
        if (node == null) {
            return 0;
        }

        // Recursively get the maximum sum of the left and right subtrees
        // If the sum is negative, we discard the subtree by considering its value as 0 (do not include in the path)
        int leftMax = Math.max(findMaxPath(node.left), 0);
        int rightMax = Math.max(findMaxPath(node.right), 0);

        // Calculate the maximum path sum through the current node (could include both left and right subtrees)
        int currentMax = node.data + leftMax + rightMax;

        // Update the global maximum path sum if the current path sum is greater
        maxSum = Math.max(maxSum, currentMax);

        // Return the maximum sum of the path that can be extended to the parent node
        return node.data + Math.max(leftMax, rightMax);
    }
    
    
    // Define the TrieNode class
    class TrieNode {
        TrieNode[] children = new TrieNode[26];
        String word = null;
    }
    
    // Define the root of the Trie
    private TrieNode root = new TrieNode();
    
    // Method to build the Trie from the list of words
    private void insertWord(String word) {
        TrieNode node = root;
        for (char c : word.toCharArray()) {
            int index = c - 'a';
            if (node.children[index] == null) {
                node.children[index] = new TrieNode();
            }
            node = node.children[index];
        }
        node.word = word;
    }
    
    // DFS method to explore the board and find words
    private void dfs(char[][] board, int i, int j, TrieNode node, List<String> result) {
        char c = board[i][j];
        
        if (c == '#' || node.children[c - 'a'] == null) {
            return;
        }
        
        node = node.children[c - 'a'];
        if (node.word != null) {
            result.add(node.word);
            node.word = null;  // Avoid duplicate entries
        }
        
        // Mark the current cell as visited
        board[i][j] = '#';
        
        // Explore the neighboring cells
        if (i > 0) dfs(board, i - 1, j, node, result);
        if (j > 0) dfs(board, i, j - 1, node, result);
        if (i < board.length - 1) dfs(board, i + 1, j, node, result);
        if (j < board[0].length - 1) dfs(board, i, j + 1, node, result);
        
        // Unmark the cell (backtrack)
        board[i][j] = c;
    }
    
    public List<String> findWords(char[][] board, String[] words) {
        List<String> result = new ArrayList<>();
        
        // Insert all words into the Trie
        for (String word : words) {
            insertWord(word);
        }
        
        // Start DFS from each cell in the board
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                dfs(board, i, j, root, result);
            }
        }
        
        return result;
    
        
    }
    
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        // Traverse the tree
        while (root != null) {
            // If both p and q are greater than root, LCA is in the right subtree
            if (p.data > root.data && q.data > root.data) {
                root = root.right;
            }
            // If both p and q are less than root, LCA is in the left subtree
            else if (p.data < root.data && q.data < root.data) {
                root = root.left;
            }
            // If p and q are on either side of root, or one is equal to root, then root is the LCA
            else {
                return root;
            }
        }
        return null; // This will never be reached if p and q are guaranteed to exist in the tree
    
}
    
}
package Tree_Practice;

class WordDictionary {

    // Define the TrieNode class
    private class TrieNode {
        TrieNode[] children = new TrieNode[26]; // 26 letters in the alphabet
        boolean isEndOfWord = false;           // Indicates the end of a valid word
    }

    private TrieNode root;

    // Constructor to initialize the WordDictionary
    public WordDictionary() {
        root = new TrieNode();
    }

    // Method to add a word to the WordDictionary
    public void addWord(String word) {
        TrieNode node = root;
        for (char c : word.toCharArray()) {
            int index = c - 'a';
            if (node.children[index] == null) {
                node.children[index] = new TrieNode();
            }
            node = node.children[index];
        }
        node.isEndOfWord = true; // Mark the end of the word
    }

    // Method to search a word in the WordDictionary
    public boolean search(String word) {
        return searchInNode(word, root);
    }

    // Helper method to perform DFS on the Trie, handling the '.' wildcard
    private boolean searchInNode(String word, TrieNode node) {
        for (int i = 0; i < word.length(); i++) {
            char c = word.charAt(i);
            if (c == '.') {
                // If the current character is '.', explore all possible paths
                for (TrieNode child : node.children) {
                    if (child != null && searchInNode(word.substring(i + 1), child)) {
                        return true;
                    }
                }
                return false;
            } else {
                // Move to the corresponding child node
                int index = c - 'a';
                if (node.children[index] == null) {
                    return false;
                }
                node = node.children[index];
            }
        }
        return node.isEndOfWord;
    }

    // Main method for testing
    public static void main(String[] args) {
        WordDictionary wordDictionary = new WordDictionary();
        wordDictionary.addWord("bad");
        wordDictionary.addWord("dad");
        wordDictionary.addWord("mad");
        
        System.out.println(wordDictionary.search("pad")); // return False
        System.out.println(wordDictionary.search("bad")); // return True
        System.out.println(wordDictionary.search(".ad")); // return True
        System.out.println(wordDictionary.search("b..")); // return True
    }
    
}

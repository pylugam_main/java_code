package Tree_Practice;

class Trie {

    // Define the TrieNode class
    private class TrieNode {
        // Each node has 26 children corresponding to each letter a-z
        private TrieNode[] children;
        // Indicates whether this node marks the end of a word
        private boolean isEndOfWord;
        
        public TrieNode() {
            children = new TrieNode[26]; // Initialize all children as null
            isEndOfWord = false;         // Initially, it's not the end of a word
        }
    }

    // Root of the Trie
    private TrieNode root;

    // Constructor to initialize the Trie
    public Trie() {
        root = new TrieNode(); // Create the root node
    }

    // Method to insert a word into the Trie
    public void insert(String word) {
        TrieNode node = root;
        for (char c : word.toCharArray()) {
            int index = c - 'a';
            if (node.children[index] == null) {
                node.children[index] = new TrieNode();
            }
            node = node.children[index];
        }
        node.isEndOfWord = true; // Mark the end of the word
    }

    // Method to search for a word in the Trie
    public boolean search(String word) {
        TrieNode node = searchNode(word);
        return node != null && node.isEndOfWord;
    }

    // Method to check if there is any word in the Trie that starts with the given prefix
    public boolean startsWith(String prefix) {
        return searchNode(prefix) != null;
    }

    // Helper method to search for a node that represents the end of the given string (word or prefix)
    private TrieNode searchNode(String str) {
        TrieNode node = root;
        for (char c : str.toCharArray()) {
            int index = c - 'a';
            if (node.children[index] == null) {
                return null;
            }
            node = node.children[index];
        }
        return node;
    }

    // Main method for testing
    public static void main(String[] args) {
        Trie trie = new Trie();
        trie.insert("apple");
        System.out.println(trie.search("apple"));   // returns true
        System.out.println(trie.search("app"));     // returns false
        System.out.println(trie.startsWith("app")); // returns true
        trie.insert("app");
        System.out.println(trie.search("app"));     // returns true
    }
}

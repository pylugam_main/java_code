package P_PriortyQueue;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class PriortyQueueTest {

//	 public ListNode mergeKLists(ListNode[] lists) {
//	        if (lists == null || lists.length == 0) {
//	            return null;
//	        }
//	        return mergeKListsHelper(lists, 0, lists.length - 1);
//	    }
//	    
//private ListNode mergeKListsHelper(ListNode[] lists, int start, int end) {
//	        if (start == end) {
//	            return lists[start];
//	        }
//	        if (start + 1 == end) {
//	            return merge(lists[start], lists[end]);
//	        }
//	        int mid = start + (end - start) / 2;
//	        ListNode left = mergeKListsHelper(lists, start, mid);
//	        ListNode right = mergeKListsHelper(lists, mid + 1, end);
//	        return merge(left, right);
//	    }
//	    
//private ListNode merge(ListNode l1, ListNode l2) {
//	        ListNode dummy = new ListNode(0);
//	        ListNode curr = dummy;
//	        
//	        while (l1 != null && l2 != null) {
//	            if (l1.val < l2.val) {
//	                curr.next = l1;
//	                l1 = l1.next;
//	            } else {
//	                curr.next = l2;
//	                l2 = l2.next;
//	            }
//	            curr = curr.next;
//	        }
//	        
//	        curr.next = (l1 != null) ? l1 : l2;
//	        
//	        return dummy.next;
//	    }


	
// sol- 2	
	
	public ListNode mergeLists(ListNode[] lists) {
		
		
		PriorityQueue<ListNode> min = 
				new PriorityQueue<ListNode>((l1,l2) -> l1.val - l2.val);
		
	for(ListNode list : lists) {
		
		if(list != null) {
			
			min.add(list);
		}
	}		
	
	ListNode dummy = new ListNode(0);
	
	ListNode current = dummy;
	
	while(!min.isEmpty()) {
		
		ListNode small = min.poll();
	
		current.next = small ;
		
		current = current.next ;
		
		if(small.next != null) {
			
			min.add(small.next);
		}
		
	}
	
	 return dummy.next ;
	
	}
	
	
public int[] topKFrequent(int[] nums, int k) {
    // Find the frequency of each number
    Map<Integer, Integer> numFrequencyMap = new HashMap<Integer, Integer>();
    for (int n : nums)
        numFrequencyMap.put(n, numFrequencyMap.getOrDefault(n, 0) + 1);

    PriorityQueue<Map.Entry<Integer, Integer>> topKElements = 
    		           new PriorityQueue<>(
            (e1, e2) -> e1.getValue() - e2.getValue());

    // Go through all numbers of the numFrequencyMap and push them into
    // topKElements, which will have
    // the top k frequent numbers. If the heap size is more than k,
    //we remove the
    // smallest (top) number.
    for (Map.Entry<Integer, Integer> entry : numFrequencyMap.entrySet()) {
        topKElements.add(entry);
        if (topKElements.size() > k) {
            topKElements.poll();
        }
    }

    // Create a list of top k numbers
    int[] topNumbers = new int[k];

    int i = 0;
    while (!topKElements.isEmpty()) {
        topNumbers[i] = topKElements.poll().getKey();
        i++;
    }

    return topNumbers;
    
       
}	

// median

private PriorityQueue<Integer> small = 
                           new PriorityQueue<>(Collections.reverseOrder());
private PriorityQueue<Integer> large = new PriorityQueue<>();
private boolean even = true;

public double findMedian() {
    if (even)
        return (small.peek() + large.peek()) / 2.0;
    else
        return small.peek();
}

public void addNum(int num) {
    if (even) {
        large.offer(num);
        small.offer(large.poll());
    } else {
        small.offer(num);
        large.offer(small.poll());
    }
    even = !even;
}


public int maxDepth(TreeNode root) {
    if (root == null) {
        return 0;
    }

    int leftSubHeight = maxDepth(root.left);
    int rightSubHeight = maxDepth(root.right);

    return Math.max(leftSubHeight, rightSubHeight) + 1;
}

}
